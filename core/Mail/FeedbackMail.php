<?php

namespace Core\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class FeedbackMail
 * @package Core\Mail
 */
class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    public $fromName;
    public $fromMail;
    public $feedbackSubject;
    public $feedbackMessage;

    /**
     * Create a new message instance.
     */
    public function __construct($fromName, $fromMail, $subject, $message)
    {
        $this->fromName = $fromName;
        $this->fromMail = $fromMail;
        $this->feedbackSubject = $subject;
        $this->feedbackMessage = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromMail, $this->fromName)
            ->subject($this->subject)
            ->view('emails.feedback', ['message' => $this->feedbackMessage]);
    }
}
