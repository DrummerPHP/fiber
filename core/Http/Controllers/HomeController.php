<?php

namespace Core\Http\Controllers;

use Core\Mail\FeedbackMail;
use Illuminate\Http\Request;
use Modules\Pages\Entities\PageEntity;

/**
 * Class HomeController
 * @package Core\Http\Controllers
 */
class HomeController extends _Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexAction()
    {
        return view('index');
    }

    public function getContactsAction()
    {
        return view('pages.contacts');
    }
}
