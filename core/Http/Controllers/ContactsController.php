<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/5/17
 * Time: 12:20
 */

namespace Core\Http\Controllers;

use Core\Http\Requests\FeedbackRequest;
use Core\Mail\FeedbackMail;
use Exception;
use Illuminate\Http\JsonResponse;
use Mail;

/**
 * Class ContactsController
 * @package Core\Http\Controllers
 */
class ContactsController extends _Controller
{

    /**
     * @param FeedbackRequest $request
     *
     * @return JsonResponse
     */
    public function postFeedbackAction(FeedbackRequest $request)
    {
        try {
            Mail::to(config('mail.to'))
                ->send(new FeedbackMail(
                        $request->get('name'),
                        $request->get('email'),
                        $request->get('subject'),
                        $request->get('message')
                    )
                );
        } catch (Exception $exception) {
            return new JsonResponse(['message' => 'false'], 200);
        }

        return new JsonResponse(['message' => 'true'], 200);
    }

}