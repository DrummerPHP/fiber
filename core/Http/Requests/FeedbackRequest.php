<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/5/17
 * Time: 19:27
 */

namespace Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FeedbackRequest
 * @package Core\Http\Requests
 */
class FeedbackRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'email' => 'required|email',
            'subject' => 'required|min:5|max:255',
            'message' => 'required|min:10|string',
        ];
    }
}