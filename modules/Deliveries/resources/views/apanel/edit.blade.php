@extends('apanel::layouts.master')

@section('styles')
    {!! Html::style('/modules/apanel/lib/summernote/summernote.css') !!}
    {!! Html::style('/modules/apanel/lib/select2/select2.css') !!}
    {!! Html::style('/modules/apanel/lib/dropzone/dropzone.css') !!}
@append

@section('page.content')

    {!! Form::open(['enctype'=>'multipart/form-data', 'url' => route('ap::deliveries:edit.post')]) !!}

    {!! Form::hidden('form[delivery][id]', ((!empty($view->data->id)) ? $view->data->id : null)) !!}

    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="tab-pane has-padding fade in"
                         id="delivery_panel">
                        <div class="form-group">
                            <label class="text-semibold">Title</label>
                            {!! Form::text('form[delivery][subject]', ((!empty($view->data->subject)) ? $view->data->subject : null), ['class' => 'form-control', 'placeholder' => 'Please enter the page title']) !!}
                        </div>

                        <div class="form-group">
                            <label class="text-semibold">Content</label>
                            {!! Form::textarea('form[delivery][content]', ((!empty($view->data->content)) ? $view->data->content : null), ['class' => 'ckeditor', ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click" data-fab-state="closed">
        <li>
            <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                <i class="fab-icon-open icon-cog"></i>
                <i class="fab-icon-close icon-cross2"></i>
            </a>

            <ul class="fab-menu-inner">
                <li>
                    <div data-fab-label="Cancel">
                        <a href="{!! url()->previous() !!}" class="btn btn-default btn-rounded btn-icon btn-float">
                            <i class="icon-undo"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Save">
                        {!! Form::button('<i class="icon-floppy-disk"></i>', ['class' => 'btn btn-default btn-rounded btn-icon btn-float', 'type' => 'submit']) !!}
                    </div>
                </li>
            </ul>
        </li>
    </ul>

    {!! Form::close() !!}

@endsection

@section('scripts')
    {!! Html::script('/modules/apanel/js/plugins/ui/fab.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/tags/tagsinput.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/tags/tokenfield.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/styling/uniform.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/ui/moment/moment.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/pickers/daterangepicker.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/editors/ckeditor_2/ckeditor.js?v=' . time()) !!}
    <script>

        $(".file-styled").uniform({
            fileButtonClass: 'action btn btn-default'
        });

        $(".token-field").tokenfield();

        @if (count(Session::get('errors')) > 0)
            @foreach ($errors->all() as $one)
                $.jGrowl('{!! $one !!}', {
            sticky: true,
            theme: 'alert-bordered alert-danger',
            header: 'Invalid data!'
        });
        @endforeach
        @endif

    </script>
@append