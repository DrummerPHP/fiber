<?php

Route::group(['middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'web'], 'namespace' => 'Modules\Deliveries\Http\Controllers'], function()
{
    Route::group(['prefix' => 'deliveries'], function () {
        Route::get('/', 'DeliveriesController@index');
        Route::post('subscribe', ['as' => 'subscribe.post', 'uses' => 'DeliveriesController@postSubscribeAction']);
    });

    Route::group(['middleware' => 'auth.apanel', 'prefix' => 'ap/deliveries'], function () {
        Route::get('send', ['as' => 'ap::deliveries:send.get', 'uses' => 'CRUDController@getSendAction']);
    });
});