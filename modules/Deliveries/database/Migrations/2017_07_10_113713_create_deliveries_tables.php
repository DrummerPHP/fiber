<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriber', function(Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('status')->default(\Modules\Deliveries\Entities\SubscriberEntity::STATUS_ACTIVE);
            $table->timestamps();
        });

        Schema::create('delivery', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriber');
        Schema::dropIfExists('delivery');
    }
}
