<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/13/17
 * Time: 21:28
 */

namespace Modules\Deliveries\Repositories;

use Illuminate\Http\Request;
use Modules\Apanel\Repositories\Repository;
use Modules\Deliveries\Entities\SubscriberEntity;
use Eloquent;
use Auth;

/**
 * Class SubscriberRepository
 * @package Modules\Deliveries\Repositories
 */
class SubscriberRepository extends Repository
{
    protected $entity = NULL;
    protected $entityName = 'subscriber';

    public function __construct()
    {
        $this->entity = new SubscriberEntity();
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param Eloquent|null $entity
     *
     * @return $this
     */
    public function setEntity(Eloquent $entity = null)
    {
        if (!$entity)
            $entity = new SubscriberEntity();

        $this->entity = $entity;

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model|mixed|SubscriberEntity|null
     */
    public function save(Request $request)
    {
        $this->entity->create(['email' => $request->input('email')]);

        return $this->entity;
    }
}