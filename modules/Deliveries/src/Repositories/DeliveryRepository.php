<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/13/17
 * Time: 18:43
 */

namespace Modules\Deliveries\Repositories;

use Illuminate\Http\Request;
use Modules\Apanel\Repositories\Repository;
use Modules\Deliveries\Entities\DeliveriesEntity;
use Eloquent;
use Auth;

/**
 * Class DeliveryRepository
 * @package Modules\Deliveries\Repositories
 */
class DeliveryRepository extends Repository
{
    protected $entity = NULL;
    protected $entityName = 'delivery';

    public function __construct()
    {
        $this->entity = new DeliveriesEntity();
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param Eloquent|null $entity
     *
     * @return $this
     */
    public function setEntity(Eloquent $entity = null)
    {
        if (!$entity)
            $entity = new DeliveriesEntity();

        $this->entity = $entity;

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model|mixed|DeliveriesEntity|null
     */
    public function save(Request $request)
    {
        $form = $request->input('form');

        if (!empty($form[$this->entityName]['id'])) {
            $this->entity = $this->entity->where('id', $form[$this->entityName]['id'])->first();
            $form[$this->entityName]['updater_id'] = Auth::user()->id;
            $this->entity->update($form[$this->entityName]);
        } else {
            $form[$this->entityName]['creator_id'] = Auth::user()->id;
            $this->entity = $this->entity->create($form[$this->entityName]);
        }

        return $this->entity;
    }
}