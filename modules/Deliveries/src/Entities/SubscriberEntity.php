<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/13/17
 * Time: 15:55
 */

namespace Modules\Deliveries\Entities;

use Eloquent;

/**
 * Class SubscriberEntity
 * @package Modules\Deliveries\Entities
 */
class SubscriberEntity extends Eloquent
{
    /**
     *
     */
    const STATUS_ACTIVE = 'ACTIVE';
    /**
     *
     */
    const STATUS_DISABLE = 'DISABLE';

    /**
     * @var string
     */
    protected $table = 'subscriber';
    /**
     * @var array
     */
    protected $fillable = [
        'email', 'status'
    ];
}