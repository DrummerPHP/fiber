<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/13/17
 * Time: 18:42
 */

namespace Modules\Deliveries\Entities;

use Eloquent;

class DeliveriesEntity extends Eloquent
{

    /**
     * @var string
     */
    protected $table = 'delivery';
    /**
     * @var array
     */
    protected $fillable = [
        'subject', 'content'
    ];

}