<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/16/17
 * Time: 15:34
 */

namespace Modules\Deliveries\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class DeliveryMail
 * @package Modules\Deliveries\Mail
 */
class DeliveryMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    protected $delivery;

    /**
     * DeliveryMail constructor.
     *
     * @param $delivery
     */
    public function __construct($delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->delivery->subject)
            ->markdown('deliveries::layouts.email')
            ->with([
                'content' => $this->delivery->content,
            ]);
    }
}