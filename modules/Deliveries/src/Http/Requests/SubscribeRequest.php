<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/5/17
 * Time: 13:56
 */

namespace Modules\Deliveries\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SubscribeRequest
 * @package Modules\Deliveries\Http\Requests
 */
class SubscribeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:subscriber,email'
        ];
    }
}