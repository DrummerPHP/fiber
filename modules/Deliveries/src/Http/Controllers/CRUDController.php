<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/10/16
 * Time: 12:19
 */

namespace Modules\Deliveries\Http\Controllers;

use Illuminate\Http\Request;
use Lviv\DataTables\Objects\Filter;
use Modules\Apanel\Http\Controllers\CRUDController as CRUDControllerInterface;
use Modules\Deliveries\Entities\SubscriberEntity;
use Modules\Deliveries\Mail\DeliveryMail;
use Modules\Deliveries\Repositories\DeliveryRepository;
use Modules\Deliveries\Http\Requests\SaveRequest;
use Yajra\Datatables\Request as DataTablesRequest;
use DataTable;
use Mail;

/**
 * Class CRUDController
 * @package Modules\Pages\Http\Controllers
 */
class CRUDController extends CRUDControllerInterface
{

    protected $_viewPath = 'deliveries::apanel';

    public function __construct()
    {
        $this->repository = new DeliveryRepository();
        $this->saveRequest = SaveRequest::class;

        parent::__construct();
    }

    public function getIndexAction(Request $request)
    {
        $this->setHeader('Deliveries', 'All deliveries');
        $this->setBreadcrumb([
            ['title' => 'Deliveries', 'href' => route('ap::deliveries:index.get')],
            ['title' => 'All deliveries'],
        ]);

        $this->dataTable = DataTable::setTableName('Deliveries')
            ->setDataUrl(route('ap::deliveries:dt.get'))
            ->setTableId('deliveries-table')
            ->addField('id', 'ID:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter the id:')
            ->addField('subject', 'Subject:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter a subject:')
            ->addField('created_at', 'Created at:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter a date:')
            ->addTools('Send', route('ap::deliveries:send.get', ['id' => '']), 'icon-play4')
            ->addTools('Edit', route('ap::deliveries:edit.get', ['id' => '']), 'icon-pencil')
            ->addTools('Delete', route('ap::deliveries:delete.get', ['id' => '']), 'icon-trash', 'confirmDelete');

        return parent::getIndexAction($request);
    }

    public function getEditAction(Request $request)
    {
        if ($request->has('id')) {
            $this->setHeader('Pages', 'Edit page');
            $this->setBreadcrumb([
                ['title' => 'Pages', 'href' => route('ap::deliveries:index.get')],
                ['title' => 'Edit'],
            ]);
        } else {
            $this->setHeader('Deliveries', 'Add delivery');
            $this->setBreadcrumb([
                ['title' => 'Deliveries', 'href' => route('ap::deliveries:index.get')],
                ['title' => 'Add'],
            ]);
        }

        return parent::getEditAction($request);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditAction(Request $request)
    {
        $request = parent::postEditAction($request);

        return $this->postEdit($request);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getSendAction(Request $request)
    {
        if ($request->get('id', '')) {

            $delivery = $this->repository->findEdit($request->get('id'))['data'];
            $subscribers = SubscriberEntity::where('status', SubscriberEntity::STATUS_ACTIVE)->get();

            Mail::to($subscribers)->queue(new DeliveryMail($delivery));

            return redirect()->back()->with('msg', [
                'type'   => 'success',
                'header' => 'Запущено',
                'text'   => 'Рассылка писем успешно запущена.',
            ]);
        }

        return redirect()->back()->with('msg', [
            'type'   => 'error',
            'header' => 'Ошибка',
            'text'   => 'Что-то пошло не так, смотрите логи сервера.',
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDeleteAction(Request $request)
    {
        return $this->getDelete($request);
    }

    /**
     * @param DataTablesRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dt(DataTablesRequest $request)
    {
        $dataTable = parent::getDataTable();

        return parent::dataTable($request);
    }

}