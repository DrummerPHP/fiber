<?php

namespace Modules\Deliveries\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Modules\Deliveries\Http\Requests\SubscribeRequest;
use Modules\Deliveries\Repositories\SubscriberRepository;

/**
 * Class DeliveriesController
 * @package Modules\Deliveries\Http\Controllers
 */
class DeliveriesController extends Controller
{
    protected $repository;

    /**
     * DeliveriesController constructor.
     */
    public function __construct()
    {
        $this->repository = new SubscriberRepository();
    }

    /**
     * @param SubscribeRequest $request
     *
     * @return JsonResponse
     */
    public function postSubscribeAction(SubscribeRequest $request)
    {
        $this->repository->save($request);

        return new JsonResponse(['message' => ''], 200);
    }
}
