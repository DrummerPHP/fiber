@extends('apanel::layouts.master')

@section('styles')
@stop

@section('page.header.title')
    @parent
    <div class="heading-elements">
        <div class="heading-btn-group">
            <a class="btn bg-teal-400" href="{!! route('ap::menu:edit.get') !!}">
                <i class="fa fa-plus-circle"></i> Add new menu item
            </a>
        </div>
    </div>
@stop

@section('page.content')
    <!-- Table tree -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Menu tree</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body"></div>

        <div class="table-responsive">
            <table class="table table-bordered tree-table">
                <colgroup>
                    <col width="30px"></col>
                    <col width="*"></col>
                    <col width="350px"></col>
                    <col width="30px"></col>
                </colgroup>
                <thead>
                <tr> <th>#</th> <th>Item</th> <th>Slug</th> <th>Tools</th> </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="panel-footer"></div>

    </div>
    <!-- /table tree -->
@endsection

@section('scripts')
    {{ Html::script("/modules/apanel/js/core/libraries/jquery_ui/core.min.js") }}
    {{ Html::script("/modules/apanel/js/core/libraries/jquery_ui/effects.min.js") }}
    {{ Html::script("/modules/apanel/js/core/libraries/jquery_ui/interactions.min.js") }}
    {{ Html::script("/modules/apanel/js/plugins/trees/fancytree_all.min.js") }}
    {{ Html::script("/modules/apanel/js/plugins/trees/fancytree_childcounter.js") }}
    {!! Html::script('/modules/apanel/js/plugins/forms/styling/uniform.min.js') !!}

    <script type="text/javascript">
        var treeTools = [
            {name: 'Edit', url: '{!! route('ap::menu:edit.get') !!}', class: 'icon-pencil'},
            {name: 'Delete', url: '{!! route('ap::menu:delete.get') !!}', class: 'icon-trash', onclick: 'confirmDelete'}
        ];

        $(".tree-table").fancytree({
            extensions: ["table"],
            table: {
                indentation: 10,      // indent 20px per node level
                nodeColumnIdx: 1      // render the node title into the 2nd column
            },
            source: {
                url: '{!! route('ap::menu:dt.get') !!}'
            },
            renderColumns: function(event, data) {
                console.log(data);
                var node = data.node,
                    $tdList = $(node.tr).find(">td");
                // (index #0 is rendered by fancytree by adding the checkbox)
                $tdList.eq(0).text(node.getIndexHier()).addClass("alignRight");
                // (index #2 is rendered by fancytree)

                var slug = (node.data.page.title !== null && node.data.page.slug !== null) ? "<a target='_blank' href='/" + node.data.page.slug + "'>" + node.data.page.title + "</a>" : "";
                var tools = ' <div class="btn-group">' +
                    '<button type="button" class="btn btn-icon btn-default dropdown-toggle" data-toggle="dropdown">' +
                    '<i class="icon-cog4"></i></button>' +
                    '<ul class="dropdown-menu dropdown-menu-right icons-left">';
                $.each(treeTools, function (index, object) {
                    tools += '<li>' +
                        '<a href="' + object.url + '?id=' + node.key + '"' + (object.onclick ? 'onclick="return ' + object.onclick + '(this)"' : '') + ' >' +
                        '<i class="' + object.class + '"></i>' + object.name +
                        '</a>' +
                        '</li>';
                });
                tools += '</ul></div>';

                $tdList.eq(2).html(slug);
                $tdList.eq(3).html(tools);
            }
        });

        var confirmDelete = function (obj) {
            if (!confirm('Вы уверены что хотите удалить?')) {
                return false;
            }
        }
    </script>
@stop