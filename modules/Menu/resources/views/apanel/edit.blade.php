@extends('apanel::layouts.master')

@section('styles')
    {!! Html::style('/modules/apanel/lib/summernote/summernote.css') !!}
    {!! Html::style('/modules/apanel/lib/select2/select2.css') !!}
    {!! Html::style('/modules/apanel/lib/dropzone/dropzone.css') !!}
    {!! Html::style('/modules/apanel/lib/dropzone/dropzone.css') !!}
@append

@section('page.content')

    {!! Form::open(['enctype'=>'multipart/form-data', 'url' => route('ap::menu:edit.post')]) !!}

    {!! Form::hidden('form[menu][id]', ((!empty($view->data->id)) ? $view->data->id : NULL)) !!}

    <div class="row">
        <div class="col-md-12">

            <div class="tabbable tab-content-bordered">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    @include('apanel::form.chunks.i18nTabs')
                    <li class="pull-right"><a href="#params_panel" data-toggle="tab"><i
                                    class="icon-cogs position-left"></i> Parameters</a></li>
                </ul>

                <div class="tab-content">

                    @php $k = 1 @endphp
                    @foreach(Localize::getSupportedLocales() as $locale => $params)

                        <div class="tab-pane has-padding fade @if ($k == 1) in active @endif"
                             id="{!! $locale !!}_panel">
                            <div class="form-group">
                                <label class="text-semibold">Title</label>
                                {!! Form::text('form[i18n][' . $locale . '][title]', ((!empty($view->data->translates[$locale]->title)) ? $view->data->translates[$locale]->title : NULL), ['class' => 'form-control', 'placeholder' => 'Please enter the page title']) !!}
                            </div>
                        </div>
                        @php $k++ @endphp
                    @endforeach
                    <div class="tab-pane has-padding fade" id="params_panel">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="text-semibold">Parent menu:</label>
                                    @include('apanel::form.autoComplete',
                                        ['field'=>[
                                            'name'=>'form[menu][parent_id]',
                                            'id'=>'parent_id',
                                            'value'=>(!empty($view->data->parent_id) ? $view->data->parent_id : ''),
                                            '_value'=>(!empty($view->data->parent->i18n->title) ? $view->data->parent->i18n->title : ''),
                                            'placeholder'=>'Change parent menu category',
                                            'key_field'=>"['id']",
                                            'value_field'=>"['i18n']['title']"
                                        ],
                                        'url'=>URL::route('ap::menu:find.post')
                                        ]
                                    )
                                </div>

                                <div class="form-group">
                                    <label class="text-semibold">Linked page:</label>
                                    @include('apanel::form.autoComplete',
                                        ['field'=>[
                                            'name' => 'form[menu][page_id]',
                                            'id' => 'page_id',
                                            'value' => (!empty($view->data->page_id) ? $view->data->page_id : ''),
                                            '_value' => (!empty($view->data->page->i18n->title) ? $view->data->page->i18n->title : ''),
                                            'placeholder' => 'Change linked page',
                                            'key_field' => "['id']",
                                            'value_field' => "['i18n']['title']"
                                        ],
                                        'url'=>URL::route('ap::pages:find.post')
                                        ]
                                    )
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group text-right">
                                    <label class="text-semibold">Visible:</label>
                                    <div class="checkbox checkbox-switchery switchery-xs">
                                        <label>
                                            {!! Form::checkbox('form[menu][visible]', 1, ((!empty($view->data->visible)) ? $view->data->visible : true), ['class' => 'switchery']) !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click" data-fab-state="closed">
        <li>
            <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                <i class="fab-icon-open icon-cog"></i>
                <i class="fab-icon-close icon-cross2"></i>
            </a>

            <ul class="fab-menu-inner">
                <li>
                    <div data-fab-label="Cancel">
                        <a href="{!! url()->previous() !!}" class="btn btn-default btn-rounded btn-icon btn-float">
                            <i class="icon-undo"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Save">
                        {!! Form::button('<i class="icon-floppy-disk"></i>', ['class' => 'btn btn-default btn-rounded btn-icon btn-float', 'type' => 'submit']) !!}
                    </div>
                </li>
            </ul>
        </li>
    </ul>

    {!! Form::close() !!}

@endsection

@section('scripts')
    {!! Html::script('/modules/apanel/js/plugins/forms/styling/switchery.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/ui/fab.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/inputs/typeahead/handlebars.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/styling/uniform.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/tags/tokenfield.min.js') !!}
    {{ Html::script("/modules/apanel/js/datatable/request.js") }}

    <script>
        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');

            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

        @if (count(Session::get('errors')) > 0)
            @foreach ($errors->all() as $one)
            $.jGrowl('{!! $one !!}', {
                sticky: true,
                theme: 'alert-bordered alert-danger',
                header: 'Invalid data!'
            });
            @endforeach
        @endif

    </script>
@append