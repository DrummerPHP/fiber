<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateMenuTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            NestedSet::columns($table);
            $table->integer('sort_id')->default(0);
            $table->integer('page_id')->unsigned()->nullable();
            $table->foreign('page_id')->references('id')->on('page')->onDelete('set null');
            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('user');
            $table->integer('updater_id')->unsigned()->nullable();
            $table->foreign('updater_id')->references('id')->on('user');
            $table->boolean('visible')->default('f');
            $table->timestamps();
        });

        Schema::create('menu_i18n', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->foreign('menu_id')->references('id')->on('menu')->onDelete('cascade');
            $table->string('locale', 2);
            $table->string('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_i18n');
        Schema::dropIfExists('menu');
    }
}
