<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/10/16
 * Time: 12:19
 */

namespace Modules\Menu\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Apanel\Http\Controllers\CRUDController as CRUDControllerInterface;
use Modules\Menu\Builders\ApanelTreeBuilder;
use Modules\Menu\Http\Requests\SaveRequest;
use Modules\Menu\Repositories\MenuRepository;
use Yajra\Datatables\Request as DataTablesRequest;

/**
 * Class CRUDController
 * @package Modules\Pages\Http\Controllers
 */
class CRUDController extends CRUDControllerInterface
{
    /**
     * @var string
     */
    protected $_viewPath = 'menu::apanel';

    public function __construct()
    {
        $this->repository = new MenuRepository();
        $this->relations = ['i18n', 'page.i18n', 'parent.i18n'];
        $this->saveRequest = SaveRequest::class;

        parent::__construct();
    }

    public function getIndexAction(Request $request)
    {
        $this->setHeader('Menu', 'All menu items');
        $this->setBreadcrumb([
            ['title' => 'Menu', 'href' => route('ap::menu:index.get')],
            ['title' => 'All menu items'],
        ]);

        return parent::getIndexAction($request);
    }

    public function getEditAction(Request $request)
    {
        if ($request->has('id')) {
            $this->setHeader('Menu', 'Edit menu item');
            $this->setBreadcrumb([
                ['title' => 'Menu', 'href' => route('ap::menu:index.get')],
                ['title' => 'Edit'],
            ]);
        } else {
            $this->setHeader('Menu', 'Add menu item');
            $this->setBreadcrumb([
                ['title' => 'Menu', 'href' => route('ap::menu:index.get')],
                ['title' => 'Add'],
            ]);
        }

        return parent::getEditAction($request);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditAction(Request $request)
    {
        $request = parent::postEditAction($request);

        return $this->postEdit($request);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getViewAction(Request $request)
    {

        $this->setHeader('Menu', 'View');
        $this->setBreadcrumb([
            ['title' => 'Menu', 'href' => route('ap::menu:index.get')],
            ['title' => 'View'],
        ]);

        return $this->getView($request);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDeleteAction(Request $request)
    {
        return $this->getDelete($request);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function postFindAction(Request $request)
    {
        return $this->postFind($request);
    }

    /**
     * @param DataTablesRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dt(DataTablesRequest $request)
    {
        return $list = json_encode(
            ApanelTreeBuilder::build($this->repository->get(['*'], ['i18n', 'page.i18n'])->toTree())
        );
    }

}