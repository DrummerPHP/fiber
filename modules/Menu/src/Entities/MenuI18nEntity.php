<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/11/17
 * Time: 18:27
 */

namespace Modules\Menu\Entities;

use Eloquent;

class MenuI18nEntity extends Eloquent
{
    protected $table = 'menu_i18n';
    protected $fillable = [
        'menu_id', 'locale', 'title'
    ];

    public $timestamps = false;

    public function menu()
    {
        return $this->belongsTo(MenuEntity::class, 'menu_id');
    }
}