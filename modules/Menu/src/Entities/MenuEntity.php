<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 7/11/17
 * Time: 18:22
 */

namespace Modules\Menu\Entities;

use Kalnoy\Nestedset\NodeTrait;
use Eloquent;
use Localize;
use Modules\Pages\Entities\PageEntity;

/**
 * Class MenuEntity
 * @package Modules\Menu\Entities
 */
class MenuEntity extends Eloquent
{
    use NodeTrait;

    const VISIBLE_TRUE = 't';
    const VISIBLE_FALSE = 'f';

    protected $primaryKey = 'id';
    protected $foreignKey = 'menu_id';
    protected $table = 'menu';
    protected $fillable = [
        '_lft', '_rgt', 'parent_id', 'sort_id', 'visible', 'creator_id', 'updater_id', 'page_id'
    ];

    public function i18n()
    {
        return $this->hasOne(MenuI18nEntity::class, $this->foreignKey)->where('locale', '=', Localize::getCurrentLocale());
    }

    public function translates()
    {
        return $this->hasMany(MenuI18nEntity::class, $this->foreignKey);
    }

    public function getTranslatesAttribute()
    {
        return $this->getRelationValue('translates')->keyBy('locale');
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', true);
    }

    public function page()
    {
        return $this->hasOne(PageEntity::class, 'id', 'page_id');
    }
}