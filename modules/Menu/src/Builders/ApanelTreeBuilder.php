<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/2/17
 * Time: 12:10
 */

namespace Modules\Menu\Builders;

class ApanelTreeBuilder
{
    public static function build($tree)
    {
        $data = [];

        foreach ($tree as $key => $item) {
            $data[$key]['key'] = $item->id;
            $data[$key]['title'] = $item->i18n->title;
            $data[$key]['page']['slug'] = (!empty($item->page)) ? $item->page->slug : null;
            $data[$key]['page']['title'] = (!empty($item->page)) ? $item->page->i18n->title : null;


            if (is_null($item->parent_id)) {
                $data[$key]['expanded'] = true;
                $data[$key]['folder'] = true;
                $data[$key]['children'] = self::build($item->children);
            }
            $data[$key] = (object) $data[$key];
        }

        return $data;
    }
}