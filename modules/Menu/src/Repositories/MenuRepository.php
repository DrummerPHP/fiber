<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 8/1/17
 * Time: 10:51
 */

namespace Modules\Menu\Repositories;

use Modules\Apanel\Repositories\Repository;
use Modules\Menu\Entities\MenuEntity;
use Illuminate\Http\Request;
use Eloquent;
use Auth;

class MenuRepository extends Repository
{
    protected $entity = NULL;
    protected $entityName = 'menu';
    protected $relations = ['i18n'];

    public function __construct()
    {
        $this->entity = new MenuEntity();
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param Eloquent|null $entity
     *
     * @return $this
     */
    public function setEntity(Eloquent $entity = null)
    {
        if (!$entity)
            $entity = new MenuEntity();

        $this->entity = $entity;

        return $this;
    }

    /**
     * @param array $fields
     * @param array $relations
     *
     * @return mixed
     */
    public function get(array $fields = ['*'], $relations = ['i18n'])
    {
        $this->entity = call_user_func([$this->entity, 'with'], $relations);

        return $this->entity->get($fields);
    }

    public function save(Request $request)
    {
        $form = $request->input('form');

        if (!empty($form[$this->entityName]['id'])) {
            $this->entity = $this->entity->where('id', $form[$this->entityName]['id'])->first();
            $form[$this->entityName]['updater_id'] = Auth::user()->id;
            $this->entity->update($form[$this->entityName]);
            foreach ($form['i18n'] as $locale => $data) {
                $this->entity->translates()->where('locale', $locale)
                    ->where('menu_id', $this->entity->id)->update($data);
            }
        } else {
            $form[$this->entityName]['creator_id'] = Auth::user()->id;
            $this->entity = $this->entity->create($form[$this->entityName]);

            foreach ($form['i18n'] as $locale => $data) {
                $this->entity->translates()->create(array_merge(['locale' => $locale], $data));
            }
        }

        return $this->entity;
    }
}