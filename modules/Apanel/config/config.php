<?php

return [

    'name' => 'Apanel',

    'modules' => [
        'Menu' => [
            'title' => 'Menu',
            'uri' => 'menu',
            'sidebar' => true,
            'icon' => 'icon-list',
            'class' => 'fa fa-list',
        ],
        'Pages' => [
            'title' => 'Pages',
            'uri' => 'pages',
            'sidebar' => true,
            'icon' => 'icon-file-xml',
            'class' => 'fa fa-file',
        ],
        'Deliveries' => [
            'title' => 'Deliveries',
            'uri' => 'deliveries',
            'sidebar' => true,
            'icon' => 'icon-envelop2',
            'class' => 'fa fa-envelope-o',
        ],
    ],

];