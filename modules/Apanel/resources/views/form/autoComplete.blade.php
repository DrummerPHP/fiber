{!! Form::text('_'.$field['name'],$field['_value'], ['class'=>'form-control','placeholder'=>$field['placeholder'],'id'=>'_'.$field['id'],])!!}
{!! Form::hidden($field['name'],$field['value'], ['class'=>'form-control','placeholder'=>$field['placeholder'],'id'=>$field['id'],])!!}
<script>
    $(function () {

        var autocompleteId = '{{$field['value']}}', autocompleteValue = '{{$field['_value']}}';

        var substringMatcher = function (strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function (i, str) {
                    if (substrRegex.test(str)) {

                        // the typeahead jQuery plugin expects suggestions to a
                        // JavaScript object, refer to typeahead docs for more info
                        matches.push({value: str});
                    }
                });

                cb(matches);
            };
        };

        // Add data
        var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];

        $('#_{!! $field['id'] !!}').typeahead({
            hint: true,
            highlight: true,
            minLength: 1,
            limit: 8
        }, {
            displayKey: 'value',
            name: 'states',
            source: function (query, processSync, processAsync) {
                return request.sendAjax('{{$url}}', {q: query, _token: '{!! csrf_token() !!}'}, function (data) {
                    var result = [];
                    if (data && !data.ajax_cancel) {
                        $.each(data, function (index, val) {
                            result.push({
                                id: val{!! $field['key_field'] !!},
                                value: val{!! $field['value_field'] !!}
                            });
                        });
                    }
                    processAsync(result);
                }, 'json', 'POST')
            }
        }).on('typeahead:selected', function (event, data) {
            $('#{!! $field['id'] !!}').val(data.id);
            var change = autocompleteId != data.id ? true : false;

            autocompleteId = data.id;
            autocompleteValue = data.value;

            if (change) {
                $('#{!! $field['id'] !!}').trigger('change');
            }
        }).on('input', function () {

        }).on('focusout', function () {
            var val = $(this).typeahead('val');
            if (val != autocompleteValue) {
                $(this).typeahead('val', '');
                $(this).val('');
                $('#{!! $field['id'] !!}').val('')
                autocompleteValue = '';
                autocompleteId = '';
                $('#{!! $field['id'] !!}').trigger('change');
            }
        });
    });
</script>