@php $k = 1 @endphp

@foreach(Localize::getSupportedLocales() as $locale => $params)
    <li class="@if ($k == 1) active @endif">
        <a href="#{!! $locale !!}_panel" data-toggle="tab">
            {!! Html::image('/modules/apanel/images/flags/'. substr($locale, 0, 2) . '.png') !!}
            {!! mb_strtoupper($locale) . ' ' . trans('apanel::tabs.content_tab_title') !!}
        </a>
    </li>
    @php $k++ @endphp
@endforeach