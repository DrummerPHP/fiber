<div class="panel panel-default">
    <div class="panel-heading">
        <h6 class="panel-title">
            <i class="{!! config('apanel.modules.' . Module::getUsedNow()->name . '.icon') !!}"></i>{!! $table->getTableName() !!}
        </h6>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                @if($table->isWithUpdateButton())
                    <li><a id="{!! $table->getTableId() !!}-update" data-action="reload"></a></li>
                @endif
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="datatable-tools table-scroll">
        <table class="table table-hover" id="{!! $table->getTableId() !!}">
            <thead>
            <tr>
                @if(count($table->getFields()))
                    @if(!empty($table->getTools()) && $table->getToolsSide() == \Lviv\DataTables\DataTables::TOOLS_SIDE_LEFT)
                        <th class="text-center">Tools</th>
                    @endif
                    @foreach($table->getFields() as $key=>$field)
                        <th class="text-center">{!! $field->getName() !!}</th>
                    @endforeach
                    @if(!empty($table->getTools()) && $table->getToolsSide() == \Lviv\DataTables\DataTables::TOOLS_SIDE_RIGHT)
                        <th class="text-center">Tools</th>
                    @endif
                @endif
            </tr>
            </thead>
            <tfoot>
            <tr>
                @if(!empty($table->getTools()) && $table->getToolsSide() == \Lviv\DataTables\DataTables::TOOLS_SIDE_LEFT)
                    <th></th>
                @endif
                @if(count($table->getFields()))
                    @foreach($table->getFields() as $key=>$field)
                        <th class="text-center">
                            @if(($filter = $field->getFilter())!=null)
                                @if($filter->getType()=='input')
                                    <input id="filter_{!! $field->getId() !!}" type="text" name="{!! $field->getKey() !!}"
                                           class="{!! (!empty($filter->getAttribute('class')) ? $filter->getAttribute('class') : 'form-control ') !!}"
                                           placeholder="{!! (!empty($filter->getAttribute('placeholder')) ? $filter->getAttribute('placeholder') : '') !!}"
                                           value="{!! $filter->getDefaultValue() !!}">
                                @elseif($filter->getType()=='select')
                                    <select id="filter_{!! $field->getId() !!}" name="{!! $field->getKey() !!}"
                                            class="{!! (!empty($filter->getAttributes()['class']) ? $filter->getAttributes()['class'] : 'form-control') !!}">
                                        <option value="">Все</option>
                                        @foreach($filter->getValues() as $value=>$text)
                                            <option value="{!! $value !!}" {!! $value == $filter->getDefaultValue() ? 'selected' : '' !!}>
                                                {!! $text !!}
                                            </option>
                                        @endforeach
                                    </select>
                                @elseif($filter->getType()=='hidden')
                                    <input id="filter_{!! $field->getId() !!}" type="hidden" name="{!! $field->getKey() !!}"
                                           value="{!! $filter->getDefaultValue() !!}">
                                @endif
                            @endif
                        </th>
                    @endforeach
                @endif
                @if(!empty($table->getTools()) && $table->getToolsSide() == \Lviv\DataTables\DataTables::TOOLS_SIDE_RIGHT)
                    <th></th>
                @endif
            </tr>
            </tfoot>
            <tbody class="text-center"></tbody>
        </table>
    </div>
</div>
@section('dataTable.script.'.$table->getTableId())
    @include('apanel::dataTable.scripts')
@show
