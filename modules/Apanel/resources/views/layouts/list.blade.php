@extends('apanel::layouts.master')

@section('styles')
@stop

@section('page.content')
@section('page.pre-table')
@show
@section('page.table')
    @if(!empty($table))
        @include('apanel::dataTable.index', ['table' => $table])
    @endif
@show
@stop

@section('scripts')
    {{ Html::script("/modules/apanel/js/plugins/tables/datatables/datatables.min.js") }}
    {{ Html::script("/modules/apanel/js/plugins/forms/selects/select2.min.js") }}
    {{ Html::script("/modules/apanel/js/plugins/pickers/datepicker.js") }}
    {{ Html::script("/modules/apanel/js/datatable/dataTable.js") }}
    {{ Html::script("/modules/apanel/js/datatable/request.js") }}
@stop