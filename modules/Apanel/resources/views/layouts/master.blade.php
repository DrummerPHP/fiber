<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>::UNEIGHT:: ADMIN</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">

{!! Html::style('/modules/apanel/css/icons/icomoon/styles.css') !!}
{!! Html::style('/modules/apanel/css/icons/fontawesome/styles.min.css') !!}
{!! Html::style('/modules/apanel/css/bootstrap.css') !!}
{!! Html::style('/modules/apanel/css/core.css') !!}
{!! Html::style('/modules/apanel/css/components.css') !!}
{!! Html::style('/modules/apanel/css/colors.css') !!}

<!-- /global stylesheets -->

    <!-- Core JS files -->
{!! Html::script('/modules/apanel/js/plugins/loaders/pace.min.js') !!}
{!! Html::script('/modules/apanel/js/core/libraries/jquery.min.js') !!}
{!! Html::script('/modules/apanel/js/core/libraries/bootstrap.min.js') !!}
{!! Html::script('/modules/apanel/js/plugins/loaders/blockui.min.js') !!}
<!-- /core JS files -->


    <!-- Theme JS files -->
{!! Html::script('/modules/apanel/js/core/app.js') !!}
<!-- /theme JS files -->

</head>

<body>

@include('apanel::layouts.chunks.navbar')

<div class="{!! request()->is('ap/login') ? 'login-container' : '' !!}">
<!-- Page container -->
    <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

    @if(!empty(request()->user()))
        @include('apanel::layouts.chunks.sidebar')
    @endif

    <!-- Main content -->
        <div class="content-wrapper">

        @section('page.header')
            @include('apanel::layouts.chunks.page-header')
        @show

        <!-- Content area -->
            <div class="content">

                @yield('page.content')

                @include('apanel::layouts.chunks.footer')

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</div>

{!! Html::script('/modules/apanel/js/plugins/notifications/jgrowl.min.js') !!}
<script>
    $(function () {
        @if(Session::has('msg'))
            <?php $msg = Session::get('msg')?>
            $(function () {
            $.jGrowl('{!! $msg['text'] !!}', {
                sticky: false,
                theme: 'alert-bordered alert-{!! !empty($msg['type']) ? $msg['type'] : 'danger' !!}',
                header: '{!! !empty($msg['header']) ? $msg['header'] : '' !!}'
            });
        });
        @endif
    });
</script>

@yield('scripts')

</body>
</html>
