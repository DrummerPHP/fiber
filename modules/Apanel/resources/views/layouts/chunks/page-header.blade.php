@section('page.header')
    @if(!empty($view->header))
        <!-- Page header -->
        <div class="page-header page-header-inverse has-cover">
            <div class="page-header-content  border-bottom-lg border-bottom-info">
                @section('page.header.title')
                    <div class="page-title">
                        <h5><i class="icon-arrow-left52 position-left"></i> <span
                                    class="text-semibold">{!! !empty($view->header['title']) ? $view->header['title'] : '' !!}</span>
                            - {!! !empty($view->header['description']) ? $view->header['description'] : '' !!}</h5>
                    </div>
                @show
            </div>

            @section('page.breadcrumb')
                @include('apanel::layouts.chunks.breadcrumb')
            @show
        </div>
        <!-- /page header -->
    @endif
@show