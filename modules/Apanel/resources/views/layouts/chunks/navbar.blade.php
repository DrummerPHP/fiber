<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="index.html">{!! Html::image('/modules/apanel/images/logo_light.png') !!}</a>

        @if(!empty(request()->user()->email))
            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        @endif
    </div>

    @if(!empty(request()->user()->email))
        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li>
                    <a class="sidebar-control sidebar-main-toggle hidden-xs">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        {!! Html::image(get_gravatar(request()->user()->email), null, ['class' => 'img-circle img-md']) !!}
                        <span>{!! request()->user()->email !!}</span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{!! route('index.get') !!}"><i class="icon-enter"></i> Go to site</a></li>
                        <li class="divider"></li>
                        <li><a href="{!! route('ap::auth:logout.get') !!}"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>

        </div>

    @endif
</div>
<!-- /main navbar -->