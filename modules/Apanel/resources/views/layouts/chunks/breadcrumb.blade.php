@section('page.breadcrumb.list')
    @if(!empty($view->breadcrumb))
        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{!! route('ap::dashboard:index.get') !!}"><i
                                class="icon-home2 position-left"></i> Dashboard</a></li>
                @if(is_array($view->breadcrumb))
                    @foreach($view->breadcrumb as $data)
                        <li>
                            @if(!empty($data['href']))
                                <a href="{{$data['href']}}">{!! !empty($data['title'])  ? $data['title'] : ''!!}</a>
                            @else
                                {!! !empty($data['title'])  ? $data['title'] : ''!!}
                            @endif
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    @endif
@show