<!-- User menu -->
<div class="sidebar-user">
    <div class="category-content">
        <div class="media">
            <a href="#"
               class="media-left">{!! Html::image(get_gravatar(request()->user()->email), null, ['class' => 'img-circle img-md']) !!}</a>
            <div class="media-body">
                <span class="media-heading text-regular">{!! mb_convert_case(request()->user()->login, MB_CASE_TITLE) !!}</span>
                <div class="text-size-mini text-muted">
                    {!! request()->user()->email !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /user menu -->