<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li class=""><a href="{!! route('ap::dashboard:index.get') !!}"><i class="icon-home4"></i>
                    <span>Dashboard</span></a></li>

            <li class="navigation-header"><span>CRUD</span> <i class="icon-menu" title="Main pages"></i></li>

            @foreach(config('apanel.modules') as $name => $params)
                @if ($params['sidebar'] === true)
                    <li class="{!! is_int(strpos(Route::currentRouteName(), 'ap::' . strtolower($name))) ? 'active' : '' !!}">
                        <a href="{!! route('ap::' . strtolower($name) . ':index.get') !!}"><i
                                    class="{!! $params['icon'] !!}"></i> <span>{!! $name !!}</span></a></li>
            @endif
        @endforeach

        {{--<li class=""><a href="{!! route('ap::dashboard:index.get') !!}"><i class="icon-file-picture"></i> <span>Gallery</span></a></li>
        <li class=""><a href="{!! route('ap::dashboard:index.get') !!}"><i class="icon-calendar2"></i> <span>Events</span></a></li>
        <li class=""><a href="{!! route('ap::dashboard:index.get') !!}"><i class="icon-music"></i> <span>Music</span></a></li>

        <li class="navigation-header"><span>Parameters</span> <i class="icon-menu" title="Main pages"></i></li>

        <li class=""><a href="{!! route('ap::dashboard:index.get') !!}"><i class="icon-cog"></i> <span>Settings</span></a></li>--}}

        <!-- /main -->

        </ul>
    </div>
</div>
<!-- /main navigation -->