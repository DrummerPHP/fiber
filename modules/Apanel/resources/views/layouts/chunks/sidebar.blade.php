<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        @include('apanel::layouts.chunks.sidebar.user')

        @include('apanel::layouts.chunks.sidebar.list')

    </div>
</div>
<!-- /main sidebar -->