@extends('apanel::layouts.master')

@section('styles')
@stop

@section('page.content')
@section('page.pre-table')
@show
@section('page.table')
    <!-- Table tree -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h6 class="panel-title">Table tree</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            The following example demonstrates rendered tree as a table (aka tree grid) and support keyboard
            navigation in a grid with embedded input controls. Table functionality is based on Fancytree's
            <code>table.js</code> extension. The tree table extension takes care of rendering the node into
            one of the columns. Other columns have to be rendered in the <code>renderColumns</code> event.
        </div>

        <div class="table-responsive">
            <table class="table table-bordered tree-table">
                <thead>
                <tr>
                    <th style="width: 46px;"></th>
                    <th style="width: 80px;">#</th>
                    <th>Items</th>
                    <th style="width: 80px;">Key</th>
                    <th style="width: 46px;">Like</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table tree -->
@show
@stop

@section('scripts')
    {{ Html::script("/modules/apanel/js/core/libraries/jquery_ui/core.min.js") }}
    {{ Html::script("/modules/apanel/js/core/libraries/jquery_ui/effects.min.js") }}
    {{ Html::script("/modules/apanel/js/core/libraries/jquery_ui/interactions.min.js") }}
    {{ Html::script("/modules/apanel/js/plugins/trees/fancytree_all.min.js") }}
    {{ Html::script("/modules/apanel/js/plugins/trees/fancytree_childcounter.js") }}
    <script>
        $(function() {
            $(".tree-table").fancytree({
                extensions: ["table"],
                checkbox: true,
                table: {
                    indentation: 20,      // indent 20px per node level
                    nodeColumnIdx: 2,     // render the node title into the 2nd column
                    checkboxColumnIdx: 0  // render the checkboxes into the 1st column
                },
                source: {
                    url: "assets/demo_data/fancytree/fancytree.json"
                },
                lazyLoad: function (event, data) {
                    data.result = {url: "ajax-sub2.json"}
                },
                renderColumns: function (event, data) {
                    var node = data.node,
                        $tdList = $(node.tr).find(">td");

                    // (index #0 is rendered by fancytree by adding the checkbox)
                    $tdList.eq(1).text(node.getIndexHier()).addClass("alignRight");

                    // (index #2 is rendered by fancytree)
                    $tdList.eq(3).text(node.key);
                    $tdList.eq(4).addClass('text-center').html("<input type='checkbox' class='styled' name='like' value='" + node.key + "'>");

                    // Style checkboxes
                    $(".styled").uniform({radioClass: 'choice'});
                }
            });

            // Handle custom checkbox clicks
            $(".tree-table").delegate("input[name=like]", "click", function (e) {
                var node = $.ui.fancytree.getNode(e),
                    $input = $(e.target);
                e.stopPropagation(); // prevent fancytree activate for this row
                if ($input.is(":checked")) {
                    alert("like " + $input.val());
                }
                else {
                    alert("dislike " + $input.val());
                }
            });
        });
    </script>
@stop