@extends('apanel::layouts.master')

@section('page.content')
    <!-- Simple login form -->

    {!! Form::open(['url' => route('ap::auth:login.post')]) !!}
    <div class="panel panel-body login-form">
        <div class="text-center">
            <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
            <h5 class="content-group">Login to your account
                <small class="display-block">Enter your credentials below</small>
            </h5>
        </div>

        <div class="form-group has-feedback has-feedback-left">
            {!! Form::text('email', null, array('class'=>'form-control')) !!}
            <div class="form-control-feedback">
                <i class="icon-user text-muted"></i>
            </div>
        </div>

        <div class="form-group has-feedback has-feedback-left">
            {!! Form::password('password', array('class'=>'form-control')) !!}
            <div class="form-control-feedback">
                <i class="icon-lock2 text-muted"></i>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Sign in <i
                        class="icon-circle-right2 position-right"></i></button>
        </div>

        <div class="text-center">
            <a href="login_password_recover.html">Forgot password?</a>
        </div>
    </div>
    {!! Form::close() !!}

    <!-- /simple login form -->
@stop