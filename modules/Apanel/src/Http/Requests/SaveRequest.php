<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/27/17
 * Time: 17:11
 */

namespace Modules\Apanel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class SaveRequest extends FormRequest
{
    abstract function rules();
}