<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/27/17
 * Time: 17:03
 */

namespace Modules\Apanel\Http\Controllers;

use Illuminate\Http\Request;
use Lviv\DataTables\Traits\ControllerTrait;
use Modules\Apanel\Http\Requests\SaveRequest;
use Modules\Apanel\Traits\CRUDTrait;
use Modules\Apanel\Traits\HeaderTrait;

abstract class CRUDController extends _Controller
{
    use HeaderTrait, CRUDTrait, ControllerTrait;

    protected $repository;
    protected $relations = [];
    protected $dataTable;
    protected $saveRequest;

    public function getIndexAction(Request $request)
    {
        return $this->addData($this->dataTable, 'table')->getIndex($request);
    }

    public function postIndexAction(Request $request)
    {

    }

    public function getEditAction(Request $request)
    {
        return $this->getEdit($request, $this->relations);
    }

    public function postEditAction(Request $request)
    {
        $request['attending'] = true;

        return app($this->saveRequest);
    }

    public function getViewAction(Request $request)
    {

    }

    public function postDeleteAction(Request $request)
    {

    }
}