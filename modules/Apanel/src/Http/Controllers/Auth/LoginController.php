<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/6/16
 * Time: 18:31
 */

namespace Modules\Apanel\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Modules\Apanel\Http\Controllers\_Controller;
use Illuminate\Http\Request;
use Modules\Apanel\Traits\HeaderTrait;

class LoginController extends _Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, HeaderTrait;

    protected $_viewPath = 'apanel::auth';

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/ap/dashboard';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth.apanel.guest', ['except' => 'getLogoutAction']);
    }

    public function getLoginAction(Request $request)
    {
        return $this->view('login');
    }

    public function postLoginAction(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function getLogoutAction(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->route('ap::auth:login.get');
    }

}