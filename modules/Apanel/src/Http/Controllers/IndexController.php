<?php

namespace Modules\Apanel\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends _Controller
{
    protected $_viewPath = 'apanel::dashboard';

    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndexAction(Request $request)
    {
        return $this->view('index');
    }
}
