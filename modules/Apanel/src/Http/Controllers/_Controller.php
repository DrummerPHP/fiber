<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/6/16
 * Time: 20:23
 */

namespace Modules\Apanel\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Modules\Apanel\Traits\ViewTrait;
use stdClass;

class _Controller extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ViewTrait;

    public function __construct()
    {
//        $this->middleware('auth.apanel');
        $this->view = new stdClass();
    }
}