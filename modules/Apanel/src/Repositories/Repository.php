<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 6/27/17
 * Time: 17:03
 */

namespace Modules\Apanel\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Eloquent;

class Repository
{
    /**
     * Entity instance.
     *
     * @var Eloquent.
     */
    protected $entity = NULL;

    /**
     * @var bool
     */
    protected $i18n = false;

    protected $relations = [];

    /**
     * @param array $fields
     *
     * @return mixed
     */
    public function get(array $fields = ['*'])
    {
        return $this->entity->get($fields);
    }

    /**
     * Get list by field.
     *
     * @param string $fieldName
     * @param mixed $value .
     *
     * @return Collection.
     */
    public function getAllByField($fieldName, $value)
    {
        if (is_array($value)) {
            $query = $this->entity->whereIn($fieldName, $value);
        } else {
            $query = $this->entity->where($fieldName, '=', $value);
        }

        return $query->get();
    }

    /**
     *
     */
    public function count()
    {

    }

    /**
     * @param null $id
     * @param array $relationsName
     *
     * @return Model
     */
    public function find($id = NULL, array $relationsName = [])
    {
        $data = NULL;

        if ($id) {
            $data = $this->entity->where('id', $id);
            if ($relationsName) {
                $data = call_user_func_array([$data, 'with'], $relationsName);
            }
            $data = $data->first();
        }

        return $data;
    }

    /**
     * @param null $id
     * @param array $relationsName
     *
     * @return array
     */
    public function findEdit($id = NULL, array $relationsName = [])
    {
        $data = NULL;

        if ($id) {
            $data = $this->entity->where('id', $id);
            if ($relationsName) {
                $data = call_user_func_array([$data, 'with'], $relationsName);
            }
            $data = $data->first();
        }

        return ['data' => $data];
    }

    public function findBy($field, $value, $relation = NULL)
    {
        if (!$relation) {
            $this->entity = $this->entity->where($field, 'ilike', '%' . $value . '%');
        } else {
            $this->entity = $this->entity->whereHas($relation, function ($query) use ($field, $value) {
                $query->where($field, 'ilike', '%' . $value . '%');
            });
        }

        return $this->entity->with($relation)->get();
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        if ($id) {
            $this->entity->where('id', '=', $id)->delete();
        }

        return true;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function save(Request $request)
    {
        $form = $request->get('form');

        if (empty($form['id'])) {
            $return = $this->entity->create($form);
        } else {
            $return = $this->entity->where('id', '=', $form['id'])
                ->update($form);
        }

        return $return;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param mixed $entity
     *
     * @return $this
     */
    public function setEntity(Eloquent $entity)
    {
        $this->entity = $entity;

        return $this;
    }
}