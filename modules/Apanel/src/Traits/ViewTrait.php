<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/6/16
 * Time: 20:31
 */

namespace Modules\Apanel\Traits;

trait ViewTrait
{
    /**
     * @var string
     */
    protected $_viewPath = 'apanel';

    /**
     * @param null $view
     * @param array $data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function view($view = NULL, $data = [])
    {
        $view = (!empty($this->_viewPath) ? $this->_viewPath . '.' . $view : $view);

        return view($view, ['view' => $this->view], $data);
    }
}