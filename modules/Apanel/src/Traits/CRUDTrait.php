<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 22/6/16
 * Time: 9:33 AM
 */

namespace Modules\Apanel\Traits;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Route;

/**
 * Class CRUD
 * @package Modules\Apanel\Traits
 */
trait CRUDTrait
{

    /**
     * @var null
     */
    protected $data = [];

    /**
     * @return null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setData(array $data = [])
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param $data
     * @param null $key
     *
     * @return $this
     */
    public function addData($data, $key = NULL)
    {
        if (!$key) {
            foreach ($data as $key => $value) {
                $this->data[$key] = $value;
            }
        } else {
            $this->data[$key] = $data;
        }

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getIndex(Request $request)
    {
        return $this->view('index', $this->data);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function postIndex(Request $request)
    {
        return $this->view('index', $this->data);
    }

    public function getView(Request $request, array $relations = [])
    {
        $id = $request->get('id', 0);
        $data = $this->repository->findEdit($id, $relations);

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $this->view->{$key} = $value;
            }
        }

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $this->view->{$key} = $value;
            }
        }

        return $this->view('view', $this->data);
    }

    /**
     * @param Request $request
     * @param array $relations
     *
     * @return mixed
     */
    public function getEdit(Request $request, array $relations = [])
    {
        $id = $request->get('id');
        $data = $this->repository->findEdit($id, $relations);

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $this->view->{$key} = $value;
            }
        }

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $this->view->{$key} = $value;
            }
        }

        return $this->view('edit', $this->data);
    }

    /**
     * @param FormRequest $request
     * @param RedirectResponse $redirect
     *
     * @return RedirectResponse
     */
    public function postEdit($request, RedirectResponse $redirect = NULL)
    {
        $response = $this->repository->save($request);

        if (!empty($response)) {
            if (!empty($response['redirect'])) {
                $redirect = $response['redirect'];
            }
        }

        if (!$redirect) {
            $route = Route::current()->getAction()['as'];
            $route = str_replace(['add.get', 'add.post', 'edit.get', 'edit.post'], 'index.get', $route);
            $redirect = redirect()->route($route);
        }

        return $redirect->with('msg', [
            'type'   => 'success',
            'text'   => 'Успешно сохранено',
            'header' => 'Сохранено',
        ]);
    }

    public function postFind(Request $request)
    {
        return $this->repository->findBy('title', $request->get('q'), 'i18n');
    }

    public function getDelete(Request $request)
    {
        if ($request->get('id', '')) {
            $this->repository->delete($request->get('id'));
        }

        return redirect()->back()->with('msg', [
            'type'   => 'success',
            'header' => 'Удалено',
            'text'   => 'Запись успешно удалена',
        ]);
    }
}