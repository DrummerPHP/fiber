<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 19:55
 */

namespace Modules\Apanel\Traits;

trait HeaderTrait
{
    public $view = NULL;

    /**
     * Set up page header title and description.
     *
     * @param $title
     * @param $description
     *
     * @return $this
     */
    public function setHeader($title, $description)
    {
        $this->view->header = [
            'title'       => $title,
            'description' => $description,
        ];

        return $this;
    }

    /**
     * Set up page breadcrumbs.
     *
     * @param array $breadcrumb
     */
    public function setBreadcrumb(array $breadcrumb = [])
    {
        if (!$breadcrumb) {
            $this->view->breadcrumb = true;
        } else {
            $this->view->breadcrumb = $breadcrumb;
        }
    }

    /**
     * Add page breadcrumbs.
     *
     * @param array $breadcrumb
     */
    public function addBreadcrumb(array $breadcrumb = [])
    {
        if (!$breadcrumb && !$this->view->breadcrumb) {
            $this->view->breadcrumb = true;
        } elseif ($breadcrumb) {
            $this->view->breadcrumb[] = $breadcrumb;
        }
    }
}