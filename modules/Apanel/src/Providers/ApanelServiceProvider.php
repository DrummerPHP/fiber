<?php

namespace Modules\Apanel\Providers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;
use Modules\Apanel\Exceptions\Handler;
use Modules\Apanel\Http\Middleware\Authenticate;
use Modules\Apanel\Http\Middleware\RedirectIfAuthenticated;
use Config;

class ApanelServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['router']->middleware('auth.apanel.guest', RedirectIfAuthenticated::class);
        $this->app['router']->middleware('auth.apanel', Authenticate::class);
        $this->app->singleton(ExceptionHandler::class, Handler::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../../config/config.php' => config_path('apanel.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'apanel'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/apanel');

        $sourcePath = __DIR__ . '/../../resources/views';

        $this->publishes([
            $sourcePath => $viewPath,
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/apanel';
        }, Config::get('view.paths')), [$sourcePath]), 'apanel');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/apanel');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'apanel');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'apanel');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
