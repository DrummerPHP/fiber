<?php

namespace Modules\Apanel\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Apanel\Entities\UserEntity;
use Hash;

class AddRootSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        UserEntity::create([
            'email'    => 'admin@webpro.net.ua',
            'password' => Hash::make('NqN2y4PANbkybiowXXpP'),
        ]);
    }
}
