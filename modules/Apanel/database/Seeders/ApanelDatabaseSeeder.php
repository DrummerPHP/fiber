<?php

namespace Modules\Apanel\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ApanelDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AddRolesSeederTableSeeder::class);
        $this->call(AddRootSeederTableSeeder::class);
    }
}
