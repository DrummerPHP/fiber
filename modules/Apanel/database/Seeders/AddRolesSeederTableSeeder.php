<?php

namespace Modules\Apanel\Database\Seeders;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AddRolesSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $roles = [
            [
                'name'         => 'root',
                'display_name' => 'Root',
                'description'  => 'Company owner',
            ],
            [
                'name'         => 'admin',
                'display_name' => 'Admin',
                'description'  => 'Company administrator',
            ],
            [
                'name'         => 'backend::manager',
                'display_name' => 'Billings',
                'description'  => 'Company billings',
            ],
        ];

        foreach ($roles as $role) {
            $role['created_at'] = Carbon::now();
            $role['updated_at'] = Carbon::now();
            DB::table('role')->insert($role);
        }
    }
}
