<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/6/16
 * Time: 20:15
 */


Route::group(['as' => 'auth:', 'namespace' => 'Auth'], function () {

    Route::get('login', ['as' => 'login.get', 'uses' => 'LoginController@getLoginAction']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'LoginController@postLoginAction']);

    Route::get('logout', ['as' => 'logout.get', 'uses' => 'LoginController@getLogoutAction']);

});