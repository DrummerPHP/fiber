<?php

Route::group(['prefix' => Localize::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'web']], function () {

    Route::group(['as' => 'ap::', 'middleware' => 'web', 'prefix' => 'ap'], function () {

        Route::group(['namespace' => 'Modules\Apanel\Http\Controllers'], function () {

            Route::group(['middleware' => 'auth.apanel'], function () {
                Route::get('/', ['as' => 'dashboard:index.get', 'uses' => 'IndexController@getIndexAction']);
                Route::get('/dashboard', ['as' => 'dashboard:index.get', 'uses' => 'IndexController@getIndexAction']);
            });

            include 'auth/login.php';
        });

        /*
         * -----------------------------------------------------------------------------------------------------------------
         *
         * Generate modules CRUD routes
         *
         * -----------------------------------------------------------------------------------------------------------------
         */

        $config = require_once(__DIR__ . '/../config/config.php');

        foreach ($config['modules'] as $name => $params) {
            Route::group(['prefix' => $params['uri'], 'as' => $params['uri'] . ':', 'namespace' => 'Modules\\' . $name . '\Http\Controllers', 'middleware' => 'auth.apanel'], function () {
                Route::get('/', ['as' => 'index.get', 'uses' => 'CRUDController@getIndexAction']);
                Route::get('dt', ['as' => 'dt.get', 'uses' => 'CRUDController@dt']);
                Route::get('edit', ['as' => 'edit.get', 'uses' => 'CRUDController@getEditAction']);
                Route::post('edit', ['as' => 'edit.post', 'uses' => 'CRUDController@postEditAction']);
                Route::get('view', ['as' => 'view.get', 'uses' => 'CRUDController@getViewAction']);
                Route::get('delete', ['as' => 'delete.get', 'uses' => 'CRUDController@getDeleteAction']);
                Route::post('find', ['as' => 'find.post', 'uses' => 'CRUDController@postFindAction']);
            });
        }

        /*
         * -----------------------------------------------------------------------------------------------------------------
         * -----------------------------------------------------------------------------------------------------------------
         * -----------------------------------------------------------------------------------------------------------------
         */

    });
});