@extends('apanel::layouts.list')

@section('page.header.title')
    @parent
    <div class="heading-elements">
        <div class="heading-btn-group">
            <a class="btn bg-teal-400" href="{!! route('ap::pages:edit.get') !!}">
                <i class="fa fa-plus-circle"></i> Add new page
            </a>
        </div>
    </div>
@stop