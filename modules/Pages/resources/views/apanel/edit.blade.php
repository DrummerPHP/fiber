@extends('apanel::layouts.master')

@section('styles')
    {!! Html::style('/modules/apanel/lib/summernote/summernote.css') !!}
    {!! Html::style('/modules/apanel/lib/select2/select2.css') !!}
    {!! Html::style('/modules/apanel/lib/dropzone/dropzone.css') !!}
@append

@section('page.content')

    {!! Form::open(['enctype'=>'multipart/form-data', 'url' => route('ap::pages:edit.post')]) !!}

    {!! Form::hidden('form[page][id]', ((!empty($view->data->id)) ? $view->data->id : null)) !!}

    <div class="row">
        <div class="col-md-12">

            <div class="tabbable tab-content-bordered">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    @include('apanel::form.chunks.i18nTabs')
                    <li class="pull-right"><a href="#params_panel" data-toggle="tab"><i
                                    class="icon-cogs position-left"></i> Parameters</a></li>
                </ul>

                <div class="tab-content">

                    @php $k = 1 @endphp
                    @foreach(Localize::getSupportedLocales() as $locale => $params)

                        <div class="tab-pane has-padding fade @if ($k == 1) in active @endif"
                             id="{!! $locale !!}_panel">
                            <div class="form-group">
                                <label class="text-semibold">Title</label>
                                {!! Form::text('form[i18n][' . $locale . '][title]', ((!empty($view->data->translates[$locale]->title)) ? $view->data->translates[$locale]->title : null), ['class' => 'form-control', 'placeholder' => 'Please enter the page title']) !!}
                            </div>

                            <div class="form-group">
                                <label class="text-semibold">Description</label>
                                {!! Form::text('form[i18n][' . $locale . '][description]', ((!empty($view->data->translates[$locale]->title)) ? $view->data->translates[$locale]->description : null), ['class' => 'form-control', 'placeholder' => 'Please enter the page description']) !!}
                            </div>

                            <div class="form-group">
                                <label class="text-semibold">Content</label>
                                {!! Form::textarea('form[i18n][' . $locale . '][content]', ((!empty($view->data->translates[$locale]->content)) ? $view->data->translates[$locale]->content : null), ['class' => 'ckeditor', 'id' => $locale . '_content']) !!}
                            </div>

                            <div class="form-group">
                                <label class="text-semibold">Meta description</label>
                                {!! Form::textarea('form[i18n][' . $locale . '][meta_description]', ((!empty($view->data->translates[$locale]->meta_description)) ? $view->data->translates[$locale]->meta_description : null), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Please enter the page short description ']) !!}
                            </div>

                            <div class="form-group">
                                <label class="text-semibold">Meta keywords:</label>
                                {!! Form::text('form[i18n][' . $locale . '][meta_keywords]', ((!empty($view->data->translates[$locale]->meta_keywords)) ? $view->data->translates[$locale]->meta_keywords : null), ['class' => 'form-control token-field']) !!}
                            </div>
                        </div>
                        @php $k++ @endphp
                    @endforeach
                    <div class="tab-pane has-padding fade" id="params_panel">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="text-semibold">Slug:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">{!! env('APP_URL') !!}/</span>
                                        {!! Form::text('form[page][slug]', ((!empty($view->data->slug)) ? $view->data->slug : null), ['class' => 'form-control', 'placeholder' => 'Enter a page slug...']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group text-right">
                                    <label class="text-semibold">Visible:</label>
                                    <div class="checkbox checkbox-switchery switchery-xs">
                                        <label>
                                            {!! Form::checkbox('form[page][visible]', 1, ((!empty($view->data->visible)) ? $view->data->visible : true), ['class' => 'switchery']) !!}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click" data-fab-state="closed">
        <li>
            <a class="fab-menu-btn btn bg-teal-400 btn-float btn-rounded btn-icon">
                <i class="fab-icon-open icon-cog"></i>
                <i class="fab-icon-close icon-cross2"></i>
            </a>

            <ul class="fab-menu-inner">
                <li>
                    <div data-fab-label="Cancel">
                        <a href="{!! url()->previous() !!}" class="btn btn-default btn-rounded btn-icon btn-float">
                            <i class="icon-undo"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div data-fab-label="Save">
                        {!! Form::button('<i class="icon-floppy-disk"></i>', ['class' => 'btn btn-default btn-rounded btn-icon btn-float', 'type' => 'submit']) !!}
                    </div>
                </li>
            </ul>
        </li>
    </ul>

    {!! Form::close() !!}

@endsection

@section('scripts')
    {!! Html::script('/modules/apanel/js/plugins/forms/styling/switchery.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/ui/fab.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/tags/tagsinput.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/tags/tokenfield.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/forms/styling/uniform.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/ui/moment/moment.min.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/pickers/daterangepicker.js') !!}
    {!! Html::script('/modules/apanel/js/plugins/editors/ckeditor_2/ckeditor.js?v=' . time()) !!}
    <script>
        if (Array.prototype.forEach) {
            var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        }
        else {
            var elems = document.querySelectorAll('.switchery');

            for (var i = 0; i < elems.length; i++) {
                var switchery = new Switchery(elems[i]);
            }
        }

        $(".file-styled").uniform({
            fileButtonClass: 'action btn btn-default'
        });

        $(".token-field").tokenfield();

        @if (count(Session::get('errors')) > 0)
            @foreach ($errors->all() as $one)
                $.jGrowl('{!! $one !!}', {
            sticky: true,
            theme: 'alert-bordered alert-danger',
            header: 'Invalid data!'
        });
        @endforeach
        @endif

    </script>
@append