@extends('layouts.app')

@section('page.meta')
    <title>{{ $page->i18n->title }}</title>
    <meta name=”description” content=”{!! $page->i18n->meta_description !!}”>
@endsection

@section('styles')
@endsection

@section('page.content')
    <!-- Begin Header Image -->
    <header class="center header-sm bg-dark" data-img="bg/smoke2.png">
        <div class="container fade-text">
            <h1 class="header-title text-center">{{ $page->i18n->title }}</h1>
            <p class="header-message text-center">{{ $page->i18n->description }}</p>
        </div>
    </header>
    <!-- End Header Image -->

    <!-- Begin  Blog Single -->
    <section class="section-blog blog-single clearfix bg-white-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="post">
                        <div class="post-text">
                            <div class="post-content">
                                {!! $page->i18n->content !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End  Blog Single -->
@endsection

@section('scripts')

@endsection