<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('page')->onDelete('set null');
            $table->integer('sort_id')->default(0);
            $table->string('slug');
            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('user');
            $table->integer('updater_id')->unsigned()->nullable();
            $table->foreign('updater_id')->references('id')->on('user');
            $table->boolean('visible')->default('f');
            $table->timestamps();
        });

        Schema::create('page_i18n', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')->references('id')->on('page')->onDelete('cascade');
            $table->string('locale', 2);
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_i18n');
        Schema::drop('page');
    }

}
