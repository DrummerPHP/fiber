<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/10/16
 * Time: 15:22
 */

namespace Modules\Pages\Repositories;

use Auth;
use Eloquent;
use Illuminate\Http\Request;
use Modules\Pages\Entities\PageEntity;
use Modules\Apanel\Repositories\Repository;

class PagesRepository extends Repository
{

    protected $entity = NULL;
    protected $entityName = 'page';
    protected $relations = ['i18n'];

    public function __construct()
    {
        $this->entity = new PageEntity();
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param Eloquent|null $entity
     *
     * @return $this
     */
    public function setEntity(Eloquent $entity = null)
    {
        if (!$entity)
            $entity = new PageEntity();

        $this->entity = $entity;

        return $this;
    }

    public function save(Request $request)
    {
        $form = $request->input('form');

        if (!empty($form[$this->entityName]['id'])) {
            $this->entity = $this->entity->where('id', $form[$this->entityName]['id'])->first();
            $form[$this->entityName]['updater_id'] = Auth::user()->id;
            $this->entity->update($form[$this->entityName]);
            foreach ($form['i18n'] as $locale => $data) {
                $this->entity->translates()->where('locale', $locale)
                    ->where('page_id', $this->entity->id)->update($data);
            }
        } else {
            $form[$this->entityName]['creator_id'] = Auth::user()->id;
            $this->entity = $this->entity->create($form[$this->entityName]);

            foreach ($form['i18n'] as $locale => $data) {
                $this->entity->translates()->create(array_merge(['locale' => $locale], $data));
            }
        }

        return $this->entity;
    }

}