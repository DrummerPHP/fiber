<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/10/16
 * Time: 12:19
 */

namespace Modules\Pages\Http\Controllers;

use Illuminate\Http\Request;
use Lviv\DataTables\Objects\Filter;
use Lviv\DataTables\Objects\Decorator;
use Modules\Apanel\Http\Controllers\CRUDController as CRUDControllerInterface;
use Modules\Pages\Http\Requests\SaveRequest;
use Modules\Pages\Repositories\PagesRepository;
use Modules\Pages\Entities\PageEntity;
use Yajra\Datatables\Request as DataTablesRequest;
use DataTable;

/**
 * Class CRUDController
 * @package Modules\Pages\Http\Controllers
 */
class CRUDController extends CRUDControllerInterface
{

    protected $_viewPath = 'pages::apanel';

    public function __construct()
    {
        $this->repository = new PagesRepository();
        $this->relations = ['i18n'];
        $this->saveRequest = SaveRequest::class;

        parent::__construct();
    }

    public function getIndexAction(Request $request)
    {
        $this->setHeader('Pages', 'All pages');
        $this->setBreadcrumb([
            ['title' => 'Pages', 'href' => route('ap::pages:index.get')],
            ['title' => 'All pages'],
        ]);

        $this->dataTable = DataTable::setTableName('Pages')
            ->setDataUrl(route('ap::pages:dt.get'))
            ->setTableId('pages-table')
            ->addField('id', 'ID:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter the id:')
            ->addField('i18n.title', 'Name:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter a title:')
            ->addField('slug', 'Slug:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter a slug:')
            ->addField('visible', 'Visible:')->setDecorator(Decorator::TYPE_BUTTON, [
                'true' => [
                    'class' => 'label label-block label-success',
                    'text' => 'Visible',
                ],
                'false' => [
                    'class' => 'label label-block label-warning',
                    'text' => 'Hidden',
                ],
            ])
            ->addFilter(Filter::TYPE_SELECT, [
                PageEntity::VISIBLE_TRUE => 'Visible',
                PageEntity::VISIBLE_FALSE => 'Hidden',
            ],'', ['class' => 'form-control full-width'])
            ->addField('created_at', 'Created at:')
            ->addFilter(Filter::TYPE_INPUT, 'Enter a title:')
//            ->addTools('View', route('ap::pages:edit.get', ['id' => '']), 'icon-eye')
            ->addTools('Edit', route('ap::pages:edit.get', ['id' => '']), 'icon-pencil')
            ->addTools('Delete', route('ap::pages:delete.get', ['id' => '']), 'icon-trash', 'confirmDelete');

        return parent::getIndexAction($request);
    }

    public function getEditAction(Request $request)
    {
        if ($request->has('id')) {
            $this->setHeader('Pages', 'Edit page');
            $this->setBreadcrumb([
                ['title' => 'Pages', 'href' => route('ap::pages:index.get')],
                ['title' => 'Edit'],
            ]);
        } else {
            $this->setHeader('Pages', 'Add page');
            $this->setBreadcrumb([
                ['title' => 'Pages', 'href' => route('ap::pages:index.get')],
                ['title' => 'Add'],
            ]);
        }

        return parent::getEditAction($request);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditAction(Request $request)
    {
        $request = parent::postEditAction($request);

        return $this->postEdit($request);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function getViewAction(Request $request)
    {

        $this->setHeader('Pages', 'View');
        $this->setBreadcrumb([
            ['title' => 'Pages', 'href' => route('ap::pages:index.get')],
            ['title' => 'View'],
        ]);

        return $this->getView($request);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDeleteAction(Request $request)
    {
        return $this->getDelete($request);
    }

    public function postFindAction(Request $request)
    {
        return $this->postFind($request);
    }

    /**
     * @param DataTablesRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dt(DataTablesRequest $request)
    {
        $dataTable = parent::getDataTable();
        $dataTable->setRelations(['i18n']);

        return parent::dataTable($request);
    }

}