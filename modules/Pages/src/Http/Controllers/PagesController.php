<?php

namespace Modules\Pages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Pages\Entities\PageEntity;

use Route;

/**
 * Class PagesController
 * @package Modules\Pages\Http\Controllers
 */
class PagesController extends Controller
{
    /**
     * @param Request $request
     * @param $slug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPageAction(Request $request, $slug)
    {
        foreach (Route::getRoutes() as $route) {
            if (str_replace(\Localize::getCurrentLocale() . '/', '', $route->uri) == $slug) {
                list ($controller, $method) = explode('@', $route->action['uses']);

                return (new $controller)->$method($request);
            }
        }

        $page = PageEntity::with('i18n')->slug($slug)->visible()->first();

        if (!$page) abort(404);

        return view('pages::index', compact('page'));
    }
}
