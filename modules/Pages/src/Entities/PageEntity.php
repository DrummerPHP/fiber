<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/10/16
 * Time: 15:21
 */

namespace Modules\Pages\Entities;

use Eloquent;
use Localize;

class PageEntity extends Eloquent
{
    const VISIBLE_TRUE = 't';
    const VISIBLE_FALSE = 'f';

    protected $primaryKey = 'id';
    protected $foreignKey = 'page_id';
    protected $table = 'page';
    protected $fillable = [
        'sort_id', 'parent_id', 'slug', 'visible', 'creator_id', 'updater_id',
    ];

    public function i18n()
    {
        return $this->hasOne(PageI18nEntity::class, $this->foreignKey)->where('locale', '=', Localize::getCurrentLocale());
    }

    public function translates()
    {
        return $this->hasMany(PageI18nEntity::class, $this->foreignKey);
    }

    public function getTranslatesAttribute()
    {
        return $this->getRelationValue('translates')->keyBy('locale');
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', true);
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
}