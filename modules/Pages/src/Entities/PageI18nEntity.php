<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/10/16
 * Time: 15:21
 */

namespace Modules\Pages\Entities;

use Eloquent;

class PageI18nEntity extends Eloquent
{
    protected $table = 'page_i18n';
    protected $fillable = [
        'page_id', 'locale', 'title', 'description', 'content', 'meta_description', 'meta_keywords'
    ];

    public $timestamps = false;

    public function page()
    {
        return $this->belongsTo(PageEntity::class, 'page_id');
    }
}