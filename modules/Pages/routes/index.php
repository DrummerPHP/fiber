<?php

Route::group(['prefix' => Localize::setLocale(), 'middleware' => ['web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'], 'namespace' => 'Modules\Pages\Http\Controllers'], function () {
    Route::get('/{slug}', ['as' => 'page.get', 'uses' => 'PagesController@getPageAction']);
});
