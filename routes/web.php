<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => Localize::setLocale(), 'middleware' => ['web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function () {

    Route::get('/', ['as' => 'index.get', 'uses' => 'HomeController@getIndexAction']);
    Route::get('contacts', ['as' => 'contacts.get', 'uses' => 'HomeController@getContactsAction']);

    Route::post('feedback', ['as' => 'feedback.post', 'uses' => 'ContactsController@postFeedbackAction']);

    Auth::routes();

});
