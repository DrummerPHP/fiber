@extends('layouts.app')

@section('page.meta')
    <title>Зворотній зв'язок - WebPro</title>
@endsection

@section('page.content')
    <!-- Begin Contact Map Full Width Dark -->
    <section class="section-contact full-map contact-wrap bg-dark height-full">
        @include('layouts.chunks.contact-form')
    </section>
    <!-- End Contact Form Map Full Width Dark -->

    @include('layouts.chunks.subscribe')

@endsection
