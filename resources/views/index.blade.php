@extends('layouts.app')

@section('page.meta')
    <title>WebPro - Універсальний інтегратор ІТ-рішень</title>
    <meta name=”description” content=”Наша компанія - це універсальний інтегратор ІТ-рішень, інженерних систем, кабельних мереж, систем безпеки, а також автоматизації та оптимізації інфраструктури Вашого бізнесу за допомогою комплексних рішень.”>
@endsection

@section('page.content')

    <!-- Begin Header Parallax Image Full Height -->
    <header class="height-full center" data-img="bg/bg-video.jpg">
        <div class="video-player mb_YTPlayer"
             data-property="{videoURL:'http://youtu.be/JRxRl3NtIEU&origin=https://webpro.ua', containment:'.height-full', startAt:0, mute:true, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:0}"></div>
        <div class="container top-element">
            <div class="row top-text fade-text">
                <h1 class="well-come">Універсальний інтегратор <br/> ІТ-рішень</h1>
                <div class="col-md-8 col-md-offset-2">
                    <div class="intro-message">● ІТ-Консалтинг ● ІТ-Аутсорсинг ● ІТ-Аутстаффинг ●</div>
                    <a href="#" class="btn button-line button-white btn-rounded hover-effect next-section m-30">Дізнатися
                        більше</a>
                </div>
            </div>
        </div>
        <div class="scroll-down next-section"><span></span></div>
    </header>
    <!-- End Header Parallax Image Full Height -->

    <!-- Begin Services 3 columns 1 row -->
    <section class="section-services services-wrap ov-hidden">
        <div class="container services p-b-20">
            <h3 class="section-title center m-b-30">Телекомунікаційні послуги</h3>
            <div class="row">
                <div class="col-md-4 service p-b-0">
                    <div class="service-desc animated" data-animation="fadeIn" data-animation-delay="600">
                        <h4 class="service-title">Інтернет</h4>
                        <p class="service-description justify">
                            При підключені до корпоративної мережі, ви завжди отримуєте: волоконно – оптичну лінію
                            зв’язку,
                            на базі власних каналів до 10Гб/с.
                        </p>
                    </div>
                    <div class="service-desc animated" data-animation="fadeIn" data-animation-delay="600">
                        <h4 class="service-title">Оренда волокон та каналів L2</h4>
                        <p class="service-description justify">
                            Виділені цифрові лінії надають Вам можливість утворити корпоративну мережу,
                            використовуючи існуючі магістральні кабелі та ємності зв’язку від оператора.
                        </p>
                    </div>
                    <div class="service-desc animated" data-animation="fadeIn" data-animation-delay="600">
                        <h4 class="service-title">Будівництво ВОЛЗ</h4>
                        <p class="service-description justify">
                            Висока кваліфікація та досвід, здатні забезпечити можливість проектування,
                            експлуатацію та обслуговування ВОЛЗ у відповідності до потреб Замовника.
                        </p>
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-1 m-t-20">
                    <div class="">
                        <figure>
                            <div class="owl-carousel" data-plugin-options='{"autoPlay": 3000}'>
                                <div class="item">
                                    <img src="/assets/images/slider/telecom/telecom-1.jpg" alt="ecommerce"
                                         class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="/assets/images/slider/telecom/telecom-2.jpg" alt="blog"
                                         class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="/assets/images/slider/telecom/telecom-3.jpg" alt="contact"
                                         class="img-responsive">
                                </div>
                            </div>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Services 3 columns 1 row -->

    <!-- Begin Call to Action Contact -->
    <section class="section-call-action action-sm bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>Маєте запитання? Наші експерти тут.</h2>
                </div>
                <div class="col-md-4 text-right m-t-0">
                    <a href="#contact-form" class="btn btn-white btn-rounded hover-effect m-b-0">Зв'язатися з нами</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call to Action Contact -->

    <!-- Begin  Projects Sortable Fullwidth -->
    <section class="section-projects clearfix">
        <div class="container">
            <div class="row">
                <div class="center m-b-30 p-t-30">
                    <h3 class="section-title">Інженерні системи</h3>
                    <p class="p-b-30">
                        ﻿Інженерні мережі представляють собою цілі комплекси і масиви рішень, що забезпечують життєздатність і комунікацію як окремого офісу, приміщення, чи будинку, так і артерій міст і країни вцілому . Це основа, що закладається в процесі будь якого будівництва об’єктів інженерного творення, на самому початку.
                    </p>
                </div>
            </div>
        </div>
        <div class="project-wrapper">
            <figure class="effect-sarah mix work-item branding animated col-md-4" data-animation="fadeIn"
                    data-animation-delay="0">
                <img src="/assets/images/blog/sks.jpg" alt="1">
                <figcaption>
                    <h2><span>Кабельні</span> мережі (СКС)</h2>
                    <p>Універсальнi телекомунікаційнi інфраструктури що забезпечують передачу сигналів всіх типів</p>
                    <a href="{!! i18n_uri('/structured-cable-networks-scn') !!}">Детальніше
                    </a>
                </figcaption>
            </figure>
            <figure class="effect-sarah mix work-item branding animated col-md-4" data-animation="fadeIn"
                    data-animation-delay="200">
                <img src="/assets/images/blog/220.jpg" alt="2">
                <figcaption>
                    <h2>Мережі <span>живлення</span> та освітлення</h2>
                    <p>Монтаж кабельних мереж, прокладання лінії освітлення з встановленням відповідних контролерів
                        керування.</p>
                    <a href="{!! i18n_uri('/power-and-lighting-networks') !!}">Детальніше</a>
                </figcaption>
            </figure>
            <figure class="effect-sarah mix work-item branding animated col-md-4" data-animation="fadeIn"
                    data-animation-delay="400">
                <img src="/assets/images/blog/connect.jpg" alt="3">
                <figcaption>
                    <h2>Тестування <span>ліній</span> звязку</h2>
                    <p>Тестування комп’ютерної та телефонної мережі, охоронних систем та інших слабострумних ліній
                        зв’язку</p>
                    <a href="{!! i18n_uri('/testing-scn-and-focl') !!}">Детальніше</a>
                </figcaption>
            </figure>
        </div>
    </section>
    <!-- End Projects Sortable Fullwidth -->

    <!-- Begin Content Discover App -->
    <section class="section-content section-drawing bg-gray-light border-bottom p-t-60 p-b-0">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left features-list">
                    <h3 class="section-title">Системи безпеки</h3>
                    <div class="media animated" data-animation="fadeIn" data-animation-delay="300">
                        <div class="pull-left icon c-primary">
                            <i class="line-icon-lock"></i>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading c-dark">Системи контролю доступу</h5>
                            <p>
                                Ми підберемо професійне, вже готове комплексне рішення, що ідеально задовольнить потреби
                                саме Вашого підприємства
                            </p>
                        </div>
                    </div>
                    <div class="media animated" data-animation="fadeIn" data-animation-delay="600">
                        <div class="pull-left icon c-primary">
                            <i class="line-icon-camcorder"></i>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading c-dark">Відеоспостереження та хмарне сховище</h5>
                            <p>
                                Ми встановимо цифрове відеоспостереження, яке працює за підтримки мережі інтернет - це
                                оптимальне рішення для сьогоднішнього покоління.
                            </p>
                        </div>
                    </div>
                    <div class="media animated" data-animation="fadeIn" data-animation-delay="900">
                        <div class="pull-left icon c-primary">
                            <i class="line-icon-fire    "></i>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading c-dark">Пожежна сигналізація і оповіщення</h5>
                            <p>
                                Обладнання протипожежного захисту автоматично виконає свою роботу, від направлення
                                тривожного імпульсу на пожежний пульт, до видалення продуктів горіння та усунення
                                небезпеки внутрішніми системами.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 p-b-40 animated" data-animation="fadeInRight" data-animation-delay="400">
                    <div id="">
                        <figure>
                            <img width="600" src="/assets/images/cctv.png" class="img-responsive m-t-40"
                                 alt="CCTV Security Camera"/>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Content Discover App -->

    <!-- Begin Services 2 columns 2 rows -->
    <section class="section-services services-wrap bg-dark">
        <div class="container services p-b-30">
            <div class="row">
                <div class="col-md-12 center">
                    <h3 class="section-title m-b-60">Що ми робимо найкраще</h3>
                </div>
                <!-- Single Service Starts -->
                <div class="col-md-6 col-sm-6 service animated" data-animation="fadeInRight" data-animation-delay="400">
                    <span class="service-icon center"><i class="icon-basic-paperplane fa-3x"></i></span>
                    <div class="service-desc">
                        <p class="service-description justify">
                            25 ти річна Системна Гарантія від Tyco Electronics / AMP NETCONNECT, що надається власнику
                            структурованої кабельної мережі. Система запроектована, інстальована та протестована у
                            відповідності з вимогами діючої редакції міжнародного ISO/IEC 11801 та/або європейського EN
                            50173 стандарту СКС, рекомендаціями і нормативними матеріалами фірми AMP
                        </p>
                    </div>
                </div>
                <!-- Single Service ends -->
                <!-- Single Service Starts -->
                <div class="col-md-6 col-sm-6 service animated" data-animation="fadeInLeft" data-animation-delay="700">
                    <span class="service-icon center"><i class="icon-basic-accelerator fa-3x"></i></span>
                    <div class="service-desc">
                        <p class="service-description justify">
                            Будівельні та монтажні роботи загального призначення, будівництво об’єктів інженерної
                            інфраструктури, електропостачання і електроосвітлення, автоматизованих систем управління,
                            виконання пусконалагоджувальних робіт, наказ Держархбудінспекції від 12.07.2017 № 29-Л на
                            здійснення діяльності з будівництва об’єктів IV і V категорій складності.
                        </p>
                    </div>
                </div>
                <!-- Single Service Ends -->
                <!-- Single Service Starts -->
                <div class="col-md-6 col-sm-6 service animated" data-animation="fadeInLeft" data-animation-delay="400">
                    <span class="service-icon center"><i class="icon-basic-book-pencil fa-3x"></i></span>
                    <div class="service-desc">
                        <p class="service-description justify">
                            Технічне обслуговування та експлуатація власних телекомунікаційних мереж. Надання доступу до
                            мережі Інтернет в Україні, згідно повідомлення Національної комісії, що здійснює державне
                            регулювання у сфері зв’язку та інформатизації.
                        </p>
                    </div>
                </div>
                <!-- Single Service Ends -->
                <!-- Single Service Starts -->
                <div class="col-md-6 col-sm-6 service animated" data-animation="fadeInRight" data-animation-delay="700">
                    <span class="service-icon center"><i class="icon-basic-lightbulb fa-3x"></i></span>
                    <div class="service-desc">
                        <p class="service-description justify">
                            Виконання небезпечних робіт в колодязях, траншеях, котлованах, колекторах. Виконання
                            верхолазних робіт на висоті 5 метрів та вище над поверхнею ґрунту, з перекриттями або
                            робочим настилом.
                        </p>
                    </div>
                </div>
                <!-- Single Service Ends -->
            </div>
        </div>
    </section>
    <!-- End Services  2 columns 2 rows -->

    <!-- Begin Content 3 columns Phone Center -->
    <section class="section-content section-phone with-swirl bg-gray-light p-b-0 overflow">
        <div class="swirl-left"><img src="/assets/images/swirl1.png" alt=""></div>
        <div class="swirl-right"><img src="/assets/images/swirl2.png" alt=""></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-left">
                    <h4 class="section-title text-center">Структуровані кабельні мережі</h4>
                    <p class="c-gray text-center">Кабельна лінія, побудована на компонентах категорії 6А, дозволяє
                        організувати
                        швидкість на передачу інформації 10Гігабіт/с., використовуючи технологію 10GbE, на відстані до
                        100 метрів.
                        з можливістю прискорення до 40 Гігабіт/с.</p>
                    <h4 class="section-title text-center">Надсучасні технології ВОЛЗ</h4>
                    <p class="c-graytext-center">При певних комбінаціях спалахів лазеру вдалось отримати швидкість
                        передачі даних більше ніж 100 терабіт в секунду, а унікальне волокно змогло
                        досягнути швидкість в 255 терабіт/с.</p>
                </div>
                <div class="col-md-6">
                    <div class="phone-carousel animated" data-animation="fadeInUp" data-animation-delay="300">
                        <figure>
                            <div class="owl-carousel phone-carousel" data-plugin-options='{"autoPlay": 2500}'>
                                <div class="item">
                                    <img src="/assets/images/slider/phone5.jpg" alt="СТРУКТУРОВАНІ КАБЕЛЬНІ МЕРЕЖІ" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="/assets/images/slider/phone1.jpg" alt="НАДСУЧАСНІ ТЕХНОЛОГІЇ ВОЛЗ" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="/assets/images/slider/phone4.jpg" alt="СИСТЕМИ БЕЗПЕКИ ТА ЗАХИСТУ" class="img-responsive">
                                </div>
                                <div class="item">
                                    <img src="/assets/images/slider/phone3.jpg" alt="НОВІТНІ СИСТЕМИ СВІТЛА ТА “СМАРТ” КЕРУВАННЯ" class="img-responsive">
                                </div>
                            </div>
                        </figure>
                        <img class="img-responsive phone-img" src="/assets/images/mockup-phone.png" alt="mobile">
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <h4 class="section-title m-t-30 text-center">﻿НОВІТНІ СИСТЕМИ СВІТЛА ТА “СМАРТ” КЕРУВАННЯ</h4>
                    <p class="c-gray text-center">Майбутнє за світлодіодними світильниками, в яких не буде
                        ніяких патронів і цоколів, оскільки хороші світло діоди живуть достатньо довго і в їх заміні
                        пропадає потреба.</p>
                    <h4 class="section-title m-t-30 text-center">Системи безпеки та захисту</h4>
                    <p class="c-gray text-center">Ми матимемо “коробочне” рішення з певним ПО, яка за допомогою
                        камер буде за всім спостерігати й сама приймати рішення про порушення периметру чи доступу, сама
                        виявить загрозу пожежі і т.д.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Content Content 3 columns Phone Center -->

    @include('layouts.chunks.subscribe')

    <!-- Begin Contact Map Full Width Dark -->
    <section class="section-contact full-map contact-wrap bg-dark">
        @include('layouts.chunks.contact-form')
    </section>
    <!-- End Contact Form Map Full Width Dark -->

    <!-- Begin  Logo List -->
    <section class="section-content clearfix p-t-30 bg-gray-light">
        <div class="container">
            <div class="row">
                <div class="center m-b-30">
                    <h3>Створіть свою високофункціональну <span class="rotate-text"
                                                                data-plugin-options='{"speed": 2000}'>
                            продуктивну, масштабовану, індивідуальну, передову, якісну</span>
                        IT мережу з нами</h3>
                    <h4 class="p-b-20">Використовуючи професійне, надійне обладнання світового рівня побудуйте і Ви
                        продуктивну IT систему з досвідченою командою та інноваційними технологіями.</h4>
                </div>
                <figure>
                    <div class="owl-carousel icon-carousel" data-plugin-options='{"items": 6, "singleItem": false}'>
                        <div class="item">
                            <img src="/assets/images/logo/cisco.png" alt="cisco">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/amp-netconnect.svg" alt="AMP NetConnect">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/hp.png" alt="hp">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/ZyXEL.png" alt="ZyXEL">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Juniper.png" alt="Juniper">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/ubiquiti.png" alt="Ubiquiti">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Intel-logo.png" alt="Intel">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Hikvision_logo.png" alt="Hikvision">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Samsung.png" alt="Samsung">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Panasonic.png" alt="Panasonic">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Molex.png" alt="Molex">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Legrand.png" alt="Legrand">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Corning.png" alt="Corning">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Panduit.png" alt="Panduit">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/Schneider.png" alt="Schneider Electric">
                        </div>
                        <div class="item">
                            <img src="/assets/images/logo/aruba_networks.gif" alt="Aruba">
                        </div>
                    </div>
                </figure>
            </div>
        </div>
    </section>
    <!--End  Logo List -->
@endsection
