<!-- Begin Footer 3 columns Dark -->
<div class="section-footer footer-wrap bg-dark">
    <div class="container footer center">
        <div class="row">
            <div class="col-md-6">
                <h4>ПРО КОМПАНІЮ</h4>
                <p>Наша компанія - це універсальний інтегратор ІТ-рішень, інженерних систем, кабельних мереж, систем
                    безпеки, а також автоматизації та оптимізації інфраструктури Вашого бізнесу за допомогою комплексних
                    рішень.</p>
            </div>
            <div class="col-md-3">
                <h4>КОНТАКТНА ІНФОРМАЦІЯ</h4>
                <p><i class="line-icon-map"></i>01042, м. Київ, вул. Філатова, б. 10-А</p>
                <p><i class="line-icon-screen-smartphone"></i>+38 ‎(044) 337-67-67</p>
                <p><i class="line-icon-envelope-open"></i><a href="mailto:noc@webpro.ua">noc@webpro.ua</a></p>
                <p><i class="line-icon-envelope-open"></i><a href="mailto:info@webpro.ua">info@webpro.ua</a></p>
                <p><i class="line-icon-calendar"></i>24/7</p>
            </div>
        </div>
    </div>
</div>
<!-- End Footer 3 columns Dark -->

<!-- Begin Copyright Dark -->
<div class="section-copyright bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p>WebPro All rights reserved &copy; 2017</p>
            </div>
            <div class="col-md-3">
                <p>Developed by <a href="https://uneight.com">Uneight project</a></p>
            </div>
        </div>
    </div>
</div>
<!-- End Copyright Dark -->
