<!-- Main Navigation menu Starts -->
<div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle top-menu" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="false">Телекомунікації</a>
            <ul class="dropdown-menu">
                <li class=""><a href="{!! i18n_uri('/internet-access') !!}">Доступ до мережі Інтернет</a></li>
                <li class=""><a href="{!! i18n_uri('/rent-fiber-channel-l2') !!}">Оренда волокон та каналів L2</a></li>
                <li class=""><a href="{!! i18n_uri('/building-an-focl') !!}">Будівництво ВОЛЗ</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle top-menu" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="false">Інженерні системи</a>
            <ul class="dropdown-menu">
                <li class=""><a href="{!! i18n_uri('/structured-cable-networks-scn') !!}">Структуровані кабельні мережі (СКС)</a></li>
                <li class=""><a href="{!! i18n_uri('/power-and-lighting-networks') !!}">Мережі живлення та освітлення</a></li>
                <li class=""><a href="{!! i18n_uri('/testing-scn-and-focl') !!}">Тестування СКС та ВОЛЗ</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle top-menu" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="false">Системи безпеки</a>
            <ul class="dropdown-menu">
                <li class=""><a href="{!! i18n_uri('/access-control-systems') !!}">Системи Контролю Доступу</a></li>
                <li class=""><a href="{!! i18n_uri('/video-surveillance-and-cloud-storage') !!}">Відеоспостереження та хмарне сховище</a></li>
                <li class=""><a href="{!! i18n_uri('/fire-alarm-alert') !!}">Пожежна Сигналізація І Оповіщення</a></li>
            </ul>
        </li>
        <li class=""><a href="{!! route('contacts.get') !!}" class="top-menu">Контакти</a>
        {{--<li class="dropdown">
            <a href="#" class="dropdown-toggle top-menu lang" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="false">EN</a>
            <ul class="dropdown-menu">
                <li><a data-lang="es" class="lang" href="#">ES</a></li>
                <li><a data-lang="fr" class="lang" href="#">FR</a></li>
                <li><a data-lang="it" class="lang" href="#">IT</a></li>
            </ul>
        </li>--}}
    </ul>
</div>
<!-- Main Navigation menu ends-->