<!-- Begin Call to Action Newsletter -->
<section class="section-call-action newsletter">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <h4 class="section-title">Залишайтеся на зв'язку.</h4>
                <p class="section-title c-gray">Ми не поширюємо вашу електронну пошту.</p>
            </div>
            <div class="col-md-5">
                {!! Form::open(['id' => 'subscribe', 'method' => 'post']) !!}
                <div class="inline-form">
                    <input placeholder="Введіть електронну адресу" name="email" class="form-control" type="text">
                    <button type="submit" class="btn btn-lg m-b-0 btn-dark subscribe-button" data-style="slide-up">
                        <span class="ladda-label">Підписатися</span>
                    </button>
                </div>

                <label id="subscribe-email-error" class="error hidden" for="email">please enter your email</label>
                {!! Form::close() !!}
                <div id="success" class="hidden">
                    <h4>Ви підписалися на розсилку новин</h4>
                    <p>Адресу вашої електронної пошти успішно додано</p>
                </div>
                <a name="contact-form"></a>
            </div>
        </div>
    </div>
</section>
<!-- End Call to Action Newsletter -->

@section('scripts')
    <script type="application/javascript">
        $(function () {
            $('#subscribe').on('submit', function (e) {
                e.preventDefault(); // prevent native submit
                var form = $(this);
                var submitButton = $('.subscribe-button').ladda();
                submitButton.ladda('start');
                $('label.error').addClass('hidden');
                $(this).ajaxSubmit({
                    dataType: 'json',
                    type: "POST",
                    url: '{{ route('subscribe.post') }}',
                    beforeSerialize: function () {
                        $(form).find('[placeholder]').each(function () {
                            var input = $(this);
                            if (input.val() == input.attr('placeholder')) {
                                input.val('');
                            }
                        })
                    },
                    success: function () {
                        $('#subscribe :input').attr('disabled', 'disabled');
                        $('#subscribe').addClass('hidden').fadeTo("slow", 0, function () {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor', 'default');
                            $('#success').removeClass('hidden').fadeIn();
                        });
                        submitButton.ladda('stop');
                    },
                    error: function (data) {
                        submitButton.ladda('stop');
                        $.each(data.responseJSON, function (index, value) {
                            $('#subscribe-' + index).addClass('error');
                            $('#subscribe-' + index + '-error').text(value).removeClass('hidden');
                        });
                        $('#contact').fadeTo("slow", 0, function () {
                            $('#error').fadeIn();
                        });
                    }
                });
            });
        });
    </script>
@append