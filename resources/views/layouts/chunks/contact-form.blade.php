@section('styles')
    <link rel="stylesheet" href="/assets/css/ladda.min.css"/>
    <link rel="stylesheet" href="/assets/css/ladda-themeless.min.css"/>
@endsection

<div class="map-container">
    <div id="full-map"></div>
</div>
<div class="container contact center animated" data-animation="fadeIn" data-animation-delay="300">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 center section-title">
            <h3 class="section-title">Залишити повідомлення</h3>
        </div>
        <div class="col-md-6">
            <div class="confirmation">
                <p><span class="fa fa-check"></span></p>
            </div>
            {!! Form::open(['class' => 'contact-form support-form', 'id' => 'feedback', 'method' => 'post']) !!}
                <div class="col-lg-12">
                    <input id="feedback-name" class="input-field form-item field-name" type="text"
                           name="name" placeholder="Ім'я" value=""/>
                    <label id="feedback-name-error" class="error hidden" for="name">please enter your email</label>

                    <input id="feedback-email" class="input-field form-item field-email" type="text"
                           name="email" placeholder="Email" value=""/>
                    <label id="feedback-email-error" class="error hidden" for="email">please enter your email</label>

                    <input id="feedback-subject" class="input-field form-item field-subject" type="text"
                           name="subject" placeholder="Тема" value=""/>
                    <label id="feedback-subject-error" class="error hidden" for="subject">please enter your email</label>

                    <textarea id="feedback-message" class="textarea-field form-item field-message" rows="3"
                              name="message" placeholder="Повідомлення"></textarea>
                    <label id="feedback-message-error" class="error hidden" for="message">please enter your email</label>
                </div>
                <button type="submit" class="btn button-line button-white btn-rounded hover-effect ladda-button" data-style="slide-up">
                    <span class="ladda-label">Надіслати повідомлення</span>
                </button>
            {!! Form::close() !!}
            <div class="success hidden">
                <h3>Ваше повідомлення успішно відправлено</h3>
                <p>Найближчим часом ми з вами обов'язково зв'яжемося</p>
            </div>
        </div>
        <div class="col-md-6 text-right">
            <h3 class="text-right section-title">БУДЕМО НА ЗВ'ЯЗКУ</h3>
            <p class="contact-description text-right c-gray">
                Напишіть нам, та отримайте швидку й точну відповідь на Ваше технічне питання від кваліфікаційного інженера , консультацію від менеджера, або фіксацію й рішення задачі відділом супроводу.
            </p>
        </div>
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('#feedback').on('submit', function(e) {
                e.preventDefault(); // prevent native submit
                var form = $(this);
                var submitButton = $('.ladda-button').ladda();
                submitButton.ladda('start');
                $('label.error').addClass('hidden');
                $(this).ajaxSubmit({
                    dataType:  'json',
                    type: "POST",
                    url: '{{ route('feedback.post') }}',
                    beforeSerialize: function() {
                        $(form).find('[placeholder]').each(function() {
                            var input = $(this);
                            if (input.val() == input.attr('placeholder')) {
                                input.val('');
                            }
                        })
                    },
                    success: function () {
                        $('#feedback :input').attr('disabled', 'disabled');
                        $('#feedback').addClass('hidden').fadeTo("slow", 0, function () {
                            $(this).find(':input').attr('disabled', 'disabled');
                            $(this).find('label').css('cursor', 'default');
                        });
                        $('.success').removeClass('hidden');
                        submitButton.ladda('stop');
                    },
                    error: function (data) {
                        $.each(data.responseJSON, function(index, value) {
                            $('#feedback-' + index).addClass('error');
                            $('#feedback-' + index + '-error').text(value).removeClass('hidden');
                        });
                        $('#contact').fadeTo("slow", 0, function () {
                            $('#error').fadeIn();
                        });
                        submitButton.ladda('stop');
                    }
                });
            });
        });
    </script>
@append