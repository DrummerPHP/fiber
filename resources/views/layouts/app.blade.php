<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    @yield('page.meta')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.png" type="image/png">
    <link rel="stylesheet" href="/assets/css/style.min.css?v={{microtime()}}"/>
    <link rel="stylesheet" id="scheme-source" href="/assets/css/schemes/gray.css"/>
    <!-- BEGIN PAGE STYLE -->
    <link rel="stylesheet" href="/assets/plugins/owl/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/plugins/bxslider/jquery.bxslider.min.css"/>
    <link rel="stylesheet" href="/assets/plugins/text-rotator/simpletextrotator.min.css">
    @yield('styles')
    <!-- END PAGE STYLE -->
    <!-- [if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif] -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-104459593-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!-- END PRELOADER -->

@include('layouts.chunks.header')

@yield('page.content')

@include('layouts.chunks.footer')

<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
<script src="/assets/plugins/bootstrap/bootstrap.min.js"></script>
<script src="/assets/plugins/modernizr/modernizr.js"></script>
<script src="/assets/plugins/appear/jquery.appear.min.js"></script>
{{--<script src="/assets/plugins/smoothscroll/smoothscroll.min.js"></script>--}}
<script src="/assets/plugins/smart-menu/jquery.smartmenus.min.js"></script>
<!-- BEGIN PAGE SCRIPTS -->
<script src="/assets/plugins/parallax/scripts/jquery.parallax-1.1.3.js"></script>
<script src="/assets/plugins/bxslider/jquery.bxslider.min.js"></script>
<script src="/assets/plugins/owl/owl.carousel.min.js"></script>
<script src="/assets/plugins/text-rotator/jquery.simple-text-rotator.min.js"></script>
<script src="https://maps.google.com/maps/api/js?sensor=true&key=AIzaSyB-Vxj4XklGMeZqwMUG3FHdsaSzRGbWQwk"></script>
<script src="/assets/plugins/google-maps/gmaps.min.js"></script>
<script src="/assets/js/markerwithlabel.js"></script>
<script src="/assets/js/map.js"></script>
<script src="/assets/plugins/drawing-svg/classie.js"></script>
<script src="/assets/plugins/drawing-svg/svganimations.js"></script>
<script src="/assets/plugins/video/jquery.mb.YTPlayer.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.1/jquery.form.js"></script>
<script src="/assets/js/spin.min.js"></script>
<script src="/assets/js/ladda.min.js"></script>
<script src="/assets/js/ladda.jquery.min.js"></script>

<script src="/assets/js/application.js"></script>
<!-- END PAGE SCRIPTS -->

@yield('scripts')

</body>
</html>
