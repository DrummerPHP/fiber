var request = {
    Request : {},
    initParams :{},
    data : {},

    sendAjax: function (url, params, callback,dataType,type,multiple, sync) {
        var _this = this;
        if (!$.isEmptyObject(this.Request) && !multiple && !this.Request.multiple) {
            this.Request.aborted = true;
            this.Request.request.abort();
        }
        var async = !sync;
        var type = type || 'POST';

        var timeout = '';

        this.Request = {
            aborted : false,
            request : null,
            multiple : multiple,
        };

        this.Request.request = $.ajax({
            url: url,
            async: async,
            type: type,
            data: params,
            dataType: dataType,
            timeout: timeout,
            success: function (json) {
                _this.apiRequest = {};
                callback(json);

            },
            error: function () {
                _this.apiRequest = {};
                callback({ajax_cancel: 1});
            }
        })
    }
};