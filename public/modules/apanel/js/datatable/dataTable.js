var _DataTable = {

    url: {},
    container: {},
    dataTable: {},
    rows: {},
    filters: {},
    tools: {},
    toolsSide: {},
    buttons: {},
    tableSettings: {},
    orders: {},

    defaultSettings: {
        order: [[0, "desc"]],
        select: true,
        processing: true,
        serverSide: true,
        ajax: {},
        columns: [],
        "bJQueryUI": false,
        "bAutoWidth": false,
        "sPaginationType": "full_numbers",
        "aLengthMenu": [25, 50, 100, 200],
        "pageLength": 25,
        "sDom": '<"datatable-header"Tfl><"datatable-scroll"t><"datatable-footer"ip>',
        "oLanguage": {
            "sSearch": '<span>Filter:</span> _INPUT_',
            "sLengthMenu": "<span>Show:</span> _MENU_",
            "oPaginate": {"sFirst": "First", "sLast": "Last", "sNext": ">", "sPrevious": "<"}
        },
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": []
        },
        "aoColumnDefs": [
            {"bSortable": true, "aTargets": [0, 1]}
        ]

    },
    columns: {},

    setUrl: function (url, tableId) {
        this.url[tableId] = url;
    },

    getUrl: function (tableId) {
        return this.url[tableId];
    },

    setContainer: function (obj, tableId) {
        this.container[tableId] = obj;
    },

    setSetting: function (key, val, tableId) {
        this.tableSettings[tableId][key] = val;
    },

    addColumn: function (data, name, bSortable, mRender, tableId,columnId) {
        bSortable = (bSortable ? true : false);
        var column = {
            data: data,
            name: name,
            bSortable: bSortable,
        };
        if (mRender && (typeof mRender == 'function')) {
            column['mRender'] = mRender;
        }
        if (!this.columns[tableId]) {
            this.columns[tableId] = [];
        }
        this.columns[tableId].push(column);
    },
    getColumns: function (tableId) {
        return this.columns[tableId] || [];
    },

    setToolsSide: function (tableId, side) {
        this.toolsSide[tableId] = (side == 'right' ? 'right' : 'left');
    },

    getToolsSide: function (tableId) {
        return this.toolsSide[tableId];
    },

    addTool: function (name, url, className, onclick, tableId, fieldName) {
        if (!this.tools[tableId]) {
            this.tools[tableId] = [];
        }
        this.tools[tableId].push({
            'name': name,
            'url': url,
            'class': className,
            'onclick': onclick,
            'fieldName': fieldName
        });
    },
    getTools: function (tableId) {
        return this.tools[tableId] || [];
    },
    renderTools: function (tableId) {
        var tools = this.getTools(tableId);
        var sideClass = this.getToolsSide(tableId) == 'left' ? 'dropdown-menu-left' : 'dropdown-menu-right';
        if (tools.length) {
            return function (data, type, full) {
                var t = ' <div class="btn-group">' +
                    '<button type="button" class="btn btn-icon btn-default dropdown-toggle" data-toggle="dropdown">' +
                    '<i class="icon-cog4"></i></button>' +
                    '<ul class="dropdown-menu ' + sideClass + ' icons-left">';
                $.each(tools, function (index, object) {
                    t += '<li>' +
                        '<a href="' + object.url + full[object.fieldName] + '" ' + (object.onclick ? 'onclick="return ' + object.onclick + '(this)"' : '') + ' >' +
                        '<i class="' + object.class + '"></i>' + object.name +
                        '</a>' +
                        '</li>';
                });
                t += '</ul></div>';
                return t;
            }
        }
        return;
    },
    addOrder: function (index, orderBy, tableId) {
        if (orderBy) {
            if (!this.orders[tableId]) {
                this.orders[tableId] = [];
            }
            orderBy = (orderBy == 'ASC' ? 'ASC' : 'DESC');
            var order = [index, orderBy];
            console.log(order, this.orders[tableId]);
            this.orders[tableId].push(order);
        }
    },
    getOrders: function (tableId) {
        return this.orders[tableId] || [];
    },
    addButton: function (extend, text, className, fnClick, tableId) {
        var button = {
            "sExtends": extend,
            "sButtonText": text,
            "sButtonClass": className,
        };
        if (fnClick && (typeof fnClick == 'function')) {
            button['fnClick'] = fnClick;
        }
        if (!this.buttons[tableId]) {
            this.buttons[tableId] = [];
        }
        this.buttons[tableId].push(button);
    },
    getButtons: function (tableId) {
        return this.buttons[tableId] || [];
    },

    init: function (tableId) {
        var _this = this;
        this.tableSettings[tableId] = {};
        $.each(this.defaultSettings, function (index, object) {
            _this.setSetting(index, object, tableId);
        });
        //console.log(this.tableSettings[tableId]);
        this.setSetting('ajax', {
            'url': this.getUrl(tableId),
            async: true
        }, tableId);

        var columns = this.getColumns(tableId);
        var renderToolsFunction = this.renderTools(tableId);
        if (renderToolsFunction) {
            switch (this.getToolsSide(tableId)) {
                case 'right':
                    columns.push({
                        data: 'id',
                        name: 'tools',
                        bSortable: false,
                        mRender: function (data, type, full) {
                            return renderToolsFunction(data, type, full)
                        }
                    });
                    break;
                case 'left' :
                    columns.unshift({
                        data: 'id',
                        name: 'tools',
                        bSortable: false,
                        mRender: function (data, type, full) {
                            return renderToolsFunction(data, type, full)
                        }
                    });
                    break;
            }
        }
        this.setSetting('columns', columns, tableId);
        this.setSetting('order', this.getOrders(tableId), tableId);
        this.setSetting('oTableTools', {
            'sRowSelect': "multi",
            'aButtons': this.getButtons(tableId)
        }, tableId);
        this.dataTable[tableId] = this.container[tableId].dataTable(this.tableSettings[tableId]);

        $.each(this.filters[tableId], function (index, object) {
            var obj = object;
            var indx = index + (_this.getToolsSide(tableId) == 'left' ? 1 : 0);
            var tag = obj.prop("tagName");
            switch (tag) {
                case 'SELECT':
                    obj.on('change', function () {
                        _this.abortAjax(tableId);
                        _this.dataTable[tableId].fnFilter(this.value, indx);
                    });
                    break;
                default:
                    obj.on('keyup', function () {
                        _this.abortAjax(tableId);
                        _this.dataTable[tableId].fnFilter(this.value, indx);
                    });
                    break;
            }
            if (object.length && object.val()) {
                _this.dataTable[tableId].fnFilter(object.val(), index);
            }
        });

    },
    getSelected: function (tableId) {
        this.rows[tableId] = this.dataTable[tableId].api().rows('.DTTT_selected').data();
    },
    getIds: function (tableId) {
        var ret = [];
        this.rows[tableId].map(function (val) {
            if (val.id) {
                ret.push(val.id);
            }
        });
        return ret;
    },
    addFilter: function (object, tableId) {
        if (!this.filters[tableId]) {
            this.filters[tableId] = [];
        }
        this.filters[tableId].push(object);
    },
    request: function (url, tableId) {
        var _this = this;
        this.getSelected(tableId);
        var params = {
            'id': this.getIds(tableId)
        };
        request.sendAjax(url, params, function (data) {
            if (data && !data.ajax_cancel) {
                $.each(data, function (index, value) {
                    $.jGrowl(value.text, {
                        sticky: false,
                        theme: 'growl-' + (value.type ? value.type : 'error'),
                        header: (value.header ? value.header : 'error'),
                        life: (value.type=='success' ? 7000 : 500000)
                    });
                });
            }
            _this.update(tableId);
        }, 'json', 'POST', true);
    },
    update: function (tableId) {
        var _this = this;
        _this.dataTable[tableId].api().ajax.reload(null, false);
    },
    abortAjax: function (tableId) {
        var all_settings = this.dataTable[tableId].DataTable().settings();
        for (var i = 0, settings; (settings = all_settings[i]); ++i) {
            if (settings.jqXHR)
                settings.jqXHR.abort();
        }
    }
};