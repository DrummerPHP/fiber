/**
 * Created by wiz-trans on 14.04.16.
 */
/**
 * Created by wiz-trans on 14.04.16.
 */
(function ($) {
    jQuery.fn.formAjax = function (method) {
        var defaultParams = {

            'successCallback': function (obj, data) {
                console.log('success', data);
            },

            'validErrorCallback': function (obj, data) {
                console.log('valid error', data);
            },

            'errorCallback': function (obj, data) {
                console.log('error', data);
            },

            ajax: {
                async: true,
                type: 'POST',
                dataType: 'json',
                timeout: 60000
            },
            'action': ''
        };


        var setParams = function (obj, params) {
            if (!obj.formAjaxParams) {
                obj.formAjaxParams = {};
            }
            obj.formAjaxParams = params;
        };

        var getParams = function (obj) {
            return (obj.formAjaxParams ? obj.formAjaxParams : {});
        };

        var doAjaxSend = function (obj, params) {
            var sendData = $(obj).serialize();
            var url = params.action;
            if (url) {
                $.ajax({
                    url: url,
                    async: params.ajax.async,
                    type: params.ajax.type,
                    data: sendData,
                    dataType: params.ajax.dataType,
                    timeout: params.ajax.timeout,
                    success: function (data) {
                        params.successCallback(obj, data);
                    },
                    error: function (data) {
                        if (data.status == 422) {
                            params.validErrorCallback(obj, (data.responseJSON || data.responseText));
                        } else {
                            params.errorCallback(obj, data);
                        }
                    }
                })
            }
        };


        var methods = {
            'init': function (params1) {
                var _this = this;
                var params = $.extend({}, defaultParams, params1);
                if (!params.action) {
                    params.action = $(_this).attr('action');
                }
                setParams(_this, params);
                $(_this).on('submit', function (e) {
                    e.preventDefault();
                    var ajaxParams = getParams(_this);
                    return doAjaxSend(_this, ajaxParams);
                });
                return _this;
            },
            'send': function () {
                var _this = this;
                var ajaxParams = getParams(_this);
                return doAjaxSend(_this, ajaxParams);
            },
            'getParams': function () {
                return getParams(this);
            },
            'setParams': function () {
                return setParams(this, params);
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist');
        }
    };
})(jQuery);